package hr.k39zzz.hwms.controller.primke;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.k39zzz.hwms.model.Primka;
import hr.k39zzz.hwms.model.PrimkaProizvod;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.PrimkaProizvodService;
import hr.k39zzz.hwms.service.PrimkaService;
import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class PrimkeController {

	@Autowired
	private PrimkaService primkaService;

	@Autowired
	private PrimkaProizvodService primkaProizvodService;

	@Autowired
	private SkladisteService skladisteService;

	@Autowired
	private ProizvodService proizvodService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/izbornik.html", method = RequestMethod.GET)
	public String getIzbornik() {
		return "izbornik";
	}

	@RequestMapping(value = "/primke/index", method = RequestMethod.GET)
	public String getLista(final Model model) {
		Primka primka1 = new Primka();
		List<Primka> primkeList = primkaService.findAllPrimka();

		model.addAttribute("primka1", primka1);
		model.addAttribute("primkeList", primkeList);
		return "primke/primkeLista";
	}

	@RequestMapping(value = "/primka/upit/nazivDobavljaca", method = RequestMethod.GET)
	public String getProizvodiSkladista(@RequestParam("nazivDobavljaca") String nazivDobavljaca, final Model model,
			Locale locale) {

		List<Primka> primkeList = primkaService.findPrimkaByDobavljac(nazivDobavljaca);
		Primka primka1 = new Primka();
		model.addAttribute("primka1", primka1);

		model.addAttribute("primkeList", primkeList);

		if (primkeList.isEmpty()) {
			model.addAttribute("poruka", messageSource.getMessage("primka.nemaPodataka", null, locale));
		}
		return "primke/primkeLista";
	}

	@RequestMapping(value = "/primke/unos", method = RequestMethod.GET)
	public String getForm(@RequestParam(required = false) Integer id, final Model model, Locale locale) {

		List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();
		model.addAttribute("skladisteList", skladisteList);

		if (id == null) {
			model.addAttribute("primkaUnos", new Primka());
			model.addAttribute("naslov", messageSource.getMessage("primka.unos.naslov", null, locale));
		} else {
			model.addAttribute("primkaUnos", primkaService.findPrimkaById(id));
			model.addAttribute("naslov", messageSource.getMessage("primka.promjena.naslov", null, locale));
		}

		return "primke/primkeUnos";
	}

	@RequestMapping(value = "/primke/unos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormPrimka() {
		return "redirect:/primke/index";
	}

	@RequestMapping(value = "/primke/unos", params = "potvrdi", method = RequestMethod.POST)
	public String potvrdiFormPrimka(@Valid @ModelAttribute("primkaUnos") final Primka primka,
			BindingResult bindingResult, Model model, Locale locale, RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();
			model.addAttribute("skladisteList", skladisteList);
			return "primke/primkeUnos";
		}

		if (!validatePrimka(primka, bindingResult)) {
			return "primke/primkeUnos";
		}
		String codePoruka = null;

		if (primka.getId() != null) {
			primkaService.updatePrimka(primka.getSifra(), primka.getNazivDobavljaca(), primka.getSkladisteId(),
					primka.getId());
			codePoruka = "primka.promjena.uspjesno";
		} else {
			primkaService.savePrimka(primka);
			codePoruka = "primka.unos.uspjesno";
		}

		redirectAttributes.addFlashAttribute("poruka", messageSource.getMessage(codePoruka, new Object[] {}, locale));

		return "redirect:/primke/proizvodi?id=" + primka.getId().toString();
	}

	@RequestMapping(value = "/primke/proizvodi", method = RequestMethod.GET)
	public String getProizvodi(@RequestParam("id") Integer primkaId, final Model model, Locale locale) {
		boolean enableEdit = true;
		List<PrimkaProizvod> primkaProizvodList = primkaService.findAllPrimkaProizvod(primkaId);

		if (primkaProizvodList.size() > 0) {
			enableEdit = false;
		}

		Primka primka = primkaService.findPrimkaById(primkaId);

		model.addAttribute("primka", primka);
		model.addAttribute("primkaProizvodList", primkaProizvodList);
		model.addAttribute("enableEdit", enableEdit);
		model.addAttribute("naslov", messageSource.getMessage("primka.proizvod.naslov", null, locale));
		return "primke/primkeProizvodi";
	}

	@RequestMapping(value = "primke/primkeProizvodUnos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormPrimkaProizvod() {
		return "redirect:/primke/index";
	}

	@RequestMapping(value = "/primke/unosPrimkeProizvoda", method = RequestMethod.GET)
	public String getFormProizvod(@RequestParam("id") int primkaId, final Model model) {
		List<PrimkaProizvod> primkaProizvodList = primkaService.findAllPrimkaProizvod(primkaId);

		Primka primka = primkaService.findPrimkaById(primkaId);
		primka.setPrimkaProizvodList(primkaProizvodList);

		List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();

		model.addAttribute("primka", primka);
		model.addAttribute("skladisteList", skladisteList);
		return "primke/primkeProizvodUnos";
	}

	private boolean validatePrimka(final Primka primka, BindingResult bindingResult) {
		if (primka.getSifra() == null || primka.getSifra().isEmpty()) {
			bindingResult.rejectValue("sifra", "primka.sifra.obavezno");
			return false;
		}

		if (primka.getNazivDobavljaca() == null || primka.getNazivDobavljaca().isEmpty()) {
			bindingResult.rejectValue("nazivDobavljaca", "primka.dobavljac.obavezno");
			return false;
		}

		return true;
	}

	@RequestMapping(value = "/primke/primkeProizvodUnos", params = "addRow", method = RequestMethod.POST)
	public String addRow(@ModelAttribute("primka") final Primka primka) {
		PrimkaProizvod pp = new PrimkaProizvod();
		List<Proizvod> sviProizvodi = proizvodService.fetchAllProizvod();
		pp.setProizvodi(sviProizvodi);

		try {
			primka.getPrimkaProizvodList().add(new PrimkaProizvod());
			for (PrimkaProizvod primkaProizvod : primka.getPrimkaProizvodList()) {
				primkaProizvod.setProizvodi(sviProizvodi);
			}
		}
		// Ako nema niti jednog proizvda u primci onda baca exception (ovo se
		// sigurno moze ljepse rijesiti)
		catch (NullPointerException e) {
			primka.setPrimkaProizvodList(new ArrayList<PrimkaProizvod>());
			primka.getPrimkaProizvodList().add(pp);
		}

		return "primke/primkeProizvodUnos";
	}

	@RequestMapping(value = "/primke/primkeProizvodUnos", params = "removeRow", method = RequestMethod.POST)
	public String removeRow(final Primka primka, final HttpServletRequest req) {
		Integer rowId = Integer.valueOf(req.getParameter("removeRow"));
		primka.getPrimkaProizvodList().remove(rowId.intValue());

		List<Proizvod> sviProizvodi = proizvodService.fetchAllProizvod();
		for (PrimkaProizvod primkaProizvod : primka.getPrimkaProizvodList()) {
			primkaProizvod.setProizvodi(sviProizvodi);
		}

		return "primke/primkeProizvodUnos";
	}

	@RequestMapping(value = "/primke/primkeProizvodUnos", params = "potvrdi", method = RequestMethod.POST)
	public String spremiPrimku(@Valid @ModelAttribute("primka") final Primka primka, Model model,
			final BindingResult bindingResult, Locale locale) {
		List<PrimkaProizvod> proizvodiPrimke = primka.getPrimkaProizvodList();

		if (bindingResult.hasErrors()) {
			return "primke/primkeProizvodUnos";
		}

		Integer primkaId = primkaService.findBySifra(primka.getSifra()).getId();
		primkaProizvodService.savePrimkaProizvodList(proizvodiPrimke, primkaId);

		model.addAttribute("primka", primka);
		model.addAttribute("primkaProizvodList", proizvodiPrimke);
		model.addAttribute("enableEdit", false);
		model.addAttribute("naslov", messageSource.getMessage("primka.proizvod.naslov", null, locale));
		return "primke/primkeProizvodi";
	}
}
