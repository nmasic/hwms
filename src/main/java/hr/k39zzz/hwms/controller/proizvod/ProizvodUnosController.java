package hr.k39zzz.hwms.controller.proizvod;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.model.VrstaProizvoda;
import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;
import hr.k39zzz.hwms.service.VrstaProizvodaService;

@Controller
public class ProizvodUnosController {

	@Autowired
	private ProizvodService proizvodService;
	@Autowired
	private SkladisteService skladisteService;

	@Autowired
	private VrstaProizvodaService vrstaProizvodaService;

	@Autowired
	private MessageSource messageSource;

	@ModelAttribute("sveVrsteProizvoda")
	public List<VrstaProizvoda> getSveVrsteProizvoda() {
		return vrstaProizvodaService.fetchAllVrstaProizvoda();
	}

	@RequestMapping(value = "/admin/proizvod/unos", method = RequestMethod.GET)
	public String getForm(@RequestParam(required = false) Integer id, final Model model, Locale locale) {
		model.addAttribute("skladista", skladisteService.fetchAllSkladiste());
		model.addAttribute("s", new Skladiste());
		if (id == null) {
			model.addAttribute("proizvodUnos", new Proizvod());
			model.addAttribute("naslov", messageSource.getMessage("proizvod.unos.naslov", null, locale));
		} else {
			model.addAttribute("proizvodUnos", proizvodService.findProizvodById(id));
			model.addAttribute("naslov", messageSource.getMessage("proizvod.promjena.naslov", null, locale));
		}

		return "proizvod/proizvodUnos";
	}

	@RequestMapping(value = "/admin/proizvod/unos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormProizvod() {
		return "redirect:/proizvod/index";
	}

	@RequestMapping(value = "/admin/proizvod/unos", params = "potvrdi", method = RequestMethod.POST)
	public String potvrdiFormProizvod(@Valid @ModelAttribute("proizvodUnos") final Proizvod proizvod,
			BindingResult bindingResult, Model model, Principal principal, Locale locale,
			RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			if (proizvod.getId() == null) {
				model.addAttribute("naslov", messageSource.getMessage("proizvod.unos.naslov", null, locale));
			} else {
				model.addAttribute("naslov", messageSource.getMessage("proizvod.promjena.naslov", null, locale));
			}
			return "proizvod/proizvodUnos";
		}

		String codePoruka = null;
		if (proizvod.getId() != null) {
			proizvod.setKorisnikIzmjene(principal.getName());
			proizvodService.saveProizvod(proizvod);
			codePoruka = "proizvod.promjena.uspjesno";
		} else {
			// prilikom kreiranja novog proizvoda
			// save-aj proizvod
			proizvod.setKorisnikKreiranja(principal.getName());
			proizvodService.saveProizvod(proizvod);
			codePoruka = "proizvod.unos.uspjesno";
		}

		redirectAttributes.addFlashAttribute("poruka", messageSource.getMessage(codePoruka, new Object[] {}, locale));

		return "redirect:/proizvod/index";
	}

}
