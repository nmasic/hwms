package hr.k39zzz.hwms.controller.proizvod;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.service.ProizvodService;

@Controller
public class ProizvodController {

	@Autowired
    private ProizvodService proizvodService;

    private static final Logger LOG = LoggerFactory.getLogger(ProizvodController.class);

	@RequestMapping(value = "/proizvod/index", method = RequestMethod.GET)
    public String getListaSkladiste(final Model model) {
	    LOG.info("Skladiste - begin action getListaSkladiste()");
        List<Proizvod> proizvodList = new ArrayList<>();
        proizvodList = proizvodService.fetchAllProizvod();
        model.addAttribute("proizvodList", proizvodList);
        return "proizvod/proizvodLista";
    }
}
