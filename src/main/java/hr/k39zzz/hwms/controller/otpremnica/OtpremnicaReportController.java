package hr.k39zzz.hwms.controller.otpremnica;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

import hr.k39zzz.hwms.model.Otpremnica;
import hr.k39zzz.hwms.model.OtpremnicaProizvod;
import hr.k39zzz.hwms.service.OtpremnicaProizvodService;
import hr.k39zzz.hwms.service.OtpremnicaService;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class OtpremnicaReportController {

	private static final String REPORT_VIEW = "classpath:jasper/otpremnica.jrxml";

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private OtpremnicaService otpremnicaService;

	@Autowired
	private OtpremnicaProizvodService otpremnicaProizvodService;

	@RequestMapping(value = "otpremnica/ispis", method = RequestMethod.GET)
	public ModelAndView getReport(@RequestParam Integer id) throws JRException, IOException {

		Otpremnica otpremnica = otpremnicaService.findOtpremnicaById(id);
		List<OtpremnicaProizvod> otpremnicaProizvodList = otpremnicaProizvodService
				.findOtpremnicaProizvodByOtpremnicaId(id);

		JasperReportsPdfView view = new JasperReportsPdfView();
		view.setReportDataKey("empty");
		view.setUrl(REPORT_VIEW);

		Map<String, Object> params = new HashMap<>();
		params.put("empty", new JREmptyDataSource());
		params.put("proizvodiDataSource", new JRBeanCollectionDataSource(otpremnicaProizvodList));
		params.put("sifra", otpremnica.getSifra());
		params.put("datumOtpremanja", otpremnica.getDatumOtpremanja());
		params.put("izdano", otpremnica.getIzdano());

		view.setApplicationContext(appContext);
		return new ModelAndView(view, params);
	}
}
