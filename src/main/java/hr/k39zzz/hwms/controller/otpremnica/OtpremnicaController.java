package hr.k39zzz.hwms.controller.otpremnica;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.k39zzz.hwms.model.Otpremnica;
import hr.k39zzz.hwms.model.OtpremnicaProizvod;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.OtpremnicaProizvodService;
import hr.k39zzz.hwms.service.OtpremnicaService;
import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class OtpremnicaController {

	@Autowired
	private OtpremnicaService otpremnicaService;

	@Autowired
	private OtpremnicaProizvodService otpremnicaProizvodService;

	@Autowired
	private SkladisteService skladisteService;

	@Autowired
	private ProizvodService proizvodService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/otpremnica/index", method = RequestMethod.GET)
	public String getLista(final Model model) {
		Otpremnica otpremnica1 = new Otpremnica();
		List<Otpremnica> otpremnicaList = otpremnicaService.findAllOtpremnica();

		model.addAttribute("otpremnica1", otpremnica1);
		model.addAttribute("otpremnicaList", otpremnicaList);
		return "otpremnica/otpremnicaLista";
	}

	@ModelAttribute("sviProizvodi")
	public List<Proizvod> getSviProizvodi() {
		return proizvodService.fetchAllProizvod();
	}

	@RequestMapping(value = "/otpremnica/upit/izdano", method = RequestMethod.GET)
	public String getProizvodiSkladista(@RequestParam("izdano") String izdano, final Model model, Locale locale) {

		Otpremnica otpremnica1 = new Otpremnica();
		List<Otpremnica> otpremnicaList = otpremnicaService.findOtpremnicaByIzdano(izdano);

		model.addAttribute("otpremnica1", otpremnica1);
		model.addAttribute("otpremnicaList", otpremnicaList);

		if (otpremnicaList.isEmpty()) {
			model.addAttribute("poruka", messageSource.getMessage("primka.nemaPodataka", null, locale));
		}
		return "otpremnica/otpremnicaLista";
	}

	@RequestMapping(value = "/otpremnica/unos", method = RequestMethod.GET)
	public String getForm(@RequestParam(required = false) Integer id, final Model model, Locale locale) {

		List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();
		model.addAttribute("skladisteList", skladisteList);

		if (id == null) {
			model.addAttribute("otpremnicaUnos", new Otpremnica());
			model.addAttribute("naslov", messageSource.getMessage("otpremnica.unos.naslov", null, locale));
		} else {
			model.addAttribute("otpremnicaUnos", otpremnicaService.findOtpremnicaById(id));
			model.addAttribute("naslov", messageSource.getMessage("otpremnica.promjena.naslov", null, locale));
		}

		return "otpremnica/otpremnicaUnos";
	}

	@RequestMapping(value = "/otpremnica/unos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormOtpremnica() {
		return "redirect:/otpremnica/index";
	}

	@RequestMapping(value = "/otpremnica/unos", params = "potvrdi", method = RequestMethod.POST)
	public String potvrdiFormPrimka(@Valid @ModelAttribute("otpremnicaUnos") final Otpremnica otpremnica,
			BindingResult bindingResult, Model model, Locale locale, RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();
			model.addAttribute("skladisteList", skladisteList);
			return "otpremnica/otpremnicaUnos";
		}

		if (!validateOtpremnica(otpremnica, bindingResult)) {
			List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();
			model.addAttribute("skladisteList", skladisteList);
			return "otpremnica/otpremnicaUnos";
		}
		String codePoruka = null;

		if (otpremnica.getId() != null) {
			otpremnicaService.updateOtpremnica(otpremnica);
			codePoruka = "primka.promjena.uspjesno";
		} else {
			otpremnicaService.saveOtpremnica(otpremnica);
			codePoruka = "primka.unos.uspjesno";
		}

		redirectAttributes.addFlashAttribute("poruka", messageSource.getMessage(codePoruka, new Object[] {}, locale));

		return "redirect:/otpremnica/index";
	}

	@RequestMapping(value = "/otpremnica/otpremnicaProizvodi", method = RequestMethod.GET)
	public String getProizvodi(@RequestParam("id") Integer otpremnicaId, final Model model, Locale locale) {
		boolean enableEdit = false;
		List<OtpremnicaProizvod> otpremnicaProizvodList = otpremnicaProizvodService
				.findOtpremnicaProizvodByOtpremnicaId(otpremnicaId);

		if (otpremnicaProizvodList.size() > 0) {
			enableEdit = true;
		}

		Otpremnica otpremnica = otpremnicaService.findOtpremnicaById(otpremnicaId);

		model.addAttribute("otpremnica", otpremnica);
		model.addAttribute("otpremnicaProizvodList", otpremnicaProizvodList);
		model.addAttribute("enableEdit", enableEdit);
		model.addAttribute("naslov", messageSource.getMessage("otpremnica.proizvod.naslov", null, locale));
		return "otpremnica/otpremnicaProizvodi";
	}

	@RequestMapping(value = "/otpremnica/stornoProizvodaOtpremnice", method = RequestMethod.GET)
	public String getStornoProizvoda(@RequestParam("id") int otpremnicaId, Locale locale,
			RedirectAttributes redirectAttributes) {
		List<OtpremnicaProizvod> otpremnicaProizvodList = otpremnicaProizvodService
				.findOtpremnicaProizvodByOtpremnicaId(otpremnicaId);
		otpremnicaProizvodService.deleteOtpremnicaProizvodList(otpremnicaProizvodList, otpremnicaId);

		redirectAttributes.addFlashAttribute("poruka",
				messageSource.getMessage("otpremnica.proizvod.storno", new Object[] {}, locale));

		return "redirect:/otpremnica/index";
	}

	@RequestMapping(value = "otpremnica/otpremnicaProizvodUnos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormOtpremnicaProizvod() {
		return "redirect:/otpremnica/index";
	}

	@RequestMapping(value = "/otpremnica/otpremnicaProizvodUnos", method = RequestMethod.GET)
	public String getFormProizvod(@RequestParam("id") int otpremnicaId, final Model model) {
		List<OtpremnicaProizvod> otpremnicaProizvodList = otpremnicaProizvodService
				.findOtpremnicaProizvodByOtpremnicaId(otpremnicaId);

		Otpremnica otpremnica = otpremnicaService.findOtpremnicaById(otpremnicaId);
		otpremnica.setOtpremnicaProizvodList(otpremnicaProizvodList);
		if (otpremnicaProizvodList.isEmpty()) {
			OtpremnicaProizvod op1 = new OtpremnicaProizvod();
			otpremnicaProizvodList.add(op1);
		}

		otpremnica.setOtpremnicaProizvodList(otpremnicaProizvodList);
		List<Skladiste> skladisteList = skladisteService.fetchAllSkladiste();

		model.addAttribute("otpremnica", otpremnica);
		model.addAttribute("skladisteList", skladisteList);
		return "otpremnica/otpremnicaProizvodUnos";
	}

	@RequestMapping(value = "/otpremnica/otpremnicaProizvodUnos", params = "addRow", method = RequestMethod.POST)
	public String addRow(@ModelAttribute("otpremnica") final Otpremnica otpremnica) {
		OtpremnicaProizvod op = new OtpremnicaProizvod();

		try {
			otpremnica.getOtpremnicaProizvodList().add(op);

		} catch (NullPointerException e) {
			otpremnica.setOtpremnicaProizvodList(new ArrayList<OtpremnicaProizvod>());
			otpremnica.getOtpremnicaProizvodList().add(op);
		}

		return "otpremnica/otpremnicaProizvodUnos";
	}

	@RequestMapping(value = "/otpremnica/otpremnicaProizvodUnos", params = "removeRow", method = RequestMethod.POST)
	public String removeRow(final Otpremnica otpremnica, final HttpServletRequest req) {
		Integer rowId = Integer.valueOf(req.getParameter("removeRow"));
		otpremnica.getOtpremnicaProizvodList().remove(rowId.intValue());

		return "otpremnica/otpremnicaProizvodUnos";
	}

	@RequestMapping(value = "/otpremnica/otpremnicaProizvodUnos", params = "potvrdi", method = RequestMethod.POST)
	public String spremiOtpremnicu(@Valid @ModelAttribute("otpremnica") final Otpremnica otpremnica,
			final BindingResult bindingResult, Model model, Locale locale) {
		List<OtpremnicaProizvod> proizvodiOtpremnice = otpremnica.getOtpremnicaProizvodList();

		if (bindingResult.hasErrors()) {
			return "otpremnica/otpremnicaProizvodUnos";
		}

		if (!validateProizvodiOtpremnice(otpremnica, bindingResult)) {
			return "otpremnica/otpremnicaProizvodUnos";
		}

		otpremnicaProizvodService.saveOtpremnicaProizvodList(proizvodiOtpremnice, otpremnica.getId());

		model.addAttribute("otpremnica", otpremnica);
		model.addAttribute("otpremnicaProizvodList", proizvodiOtpremnice);
		model.addAttribute("enableEdit", false);
		model.addAttribute("naslov", messageSource.getMessage("otpremnica.proizvod.naslov", null, locale));
		return "otpremnica/otpremnicaProizvodi";
	}

	private boolean validateOtpremnica(final Otpremnica otpremnica, BindingResult bindingResult) {
		if (otpremnica.getSifra() == null || otpremnica.getSifra().isEmpty()) {
			bindingResult.rejectValue("sifra", "primka.sifra.obavezno");
			return false;
		}

		if (otpremnica.getIzdano() == null || otpremnica.getIzdano().isEmpty()) {
			bindingResult.rejectValue("izdano", "otpremnica.izdano.obavezno");
			return false;
		}

		if (otpremnicaService.findBySifra(otpremnica.getSifra()) != null) {
			bindingResult.rejectValue("sifra", "otpreminca.sifra.postoji");
			return false;
		}

		return true;
	}

	private boolean validateProizvodiOtpremnice(final Otpremnica otpremnica, BindingResult bindingResult) {
		for (OtpremnicaProizvod op2 : otpremnica.getOtpremnicaProizvodList()) {
			if (op2.getKolicina() == null || op2.getKolicina() == 0 || op2.getKolicina() <= 0) {
				bindingResult.reject("otpremnica.kolicina.obavezno");
				return false;
			}
		}
		return true;
	}
}
