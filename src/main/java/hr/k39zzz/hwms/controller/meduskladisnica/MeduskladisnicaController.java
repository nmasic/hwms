package hr.k39zzz.hwms.controller.meduskladisnica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hr.k39zzz.hwms.dao.MeduskladisnicaProizvodRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.model.Meduskladisnica;
import hr.k39zzz.hwms.model.MeduskladisnicaProizvod;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.model.VrstaProizvoda;
import hr.k39zzz.hwms.model.dto.MeduskladisnicaDto;
import hr.k39zzz.hwms.model.dto.ProizvodPrenesiDto;
import hr.k39zzz.hwms.model.dto.ProizvodPrenesiDtoWrapper;
import hr.k39zzz.hwms.model.dto.ProizvodStanjeDto;
import hr.k39zzz.hwms.service.MeduskladisnicaProizvodService;
import hr.k39zzz.hwms.service.MeduskladisnicaService;
import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;
import hr.k39zzz.hwms.service.VrstaProizvodaService;
import hr.k39zzz.hwms.service.impl.MeduskladisnicaServiceImpl;

@Controller
public class MeduskladisnicaController {
	
	@Autowired
	MeduskladisnicaService meduskladisnicaService;
	@Autowired
	MeduskladisnicaProizvodService mpService;
	@Autowired
	SkladisteService skladisteService;
	@Autowired
	SkladisteProizvodService skladisteProizvodService;
	@Autowired
	ProizvodService proizvodService;
	@Autowired
	VrstaProizvodaService vrstaProizvodaService;
	
	
	@RequestMapping(value="/meduskladisnice/lista")
	public String showAllMeduskladisnica(Model model){
		
		model.addAttribute("meduskladisnice",meduskladisnicaService.fetchAllMeduskladisnica());
	   return "meduskladisnice/listaMeduskladisnica";	
		
	}
	@RequestMapping(value="/meduskladisnica/detalji")
public String prikaziDetaljeMeduskladisnice(@RequestParam(required = true) Integer id,Model model){
	      
		
		
		
		
		
		
		Meduskladisnica m=meduskladisnicaService.findMeduskladisnicaById(id);
		Skladiste sourceSkladiste=skladisteService.fetchSkladisteById(m.getSourceSkladiste().getId());
		Skladiste targetSkladiste=skladisteService.fetchSkladisteById(m.getTargetSkladiste().getId());
		model.addAttribute("meduskladisnica",meduskladisnicaService.findMeduskladisnicaById(id));
		model.addAttribute("sourceSkladiste", sourceSkladiste);
		model.addAttribute("targetSkladiste", targetSkladiste);
		
		
		List<Object> genericList=mpService.fetchProizvodiByMeduskladisnicaId(id);
		 List<String> listaProizvoda=(List<String>)(genericList.get(0));
		 List<Integer> listaKolicina=(List<Integer>)(genericList.get(1));
		

		model.addAttribute("listaProizvoda", listaProizvoda);
		model.addAttribute("listaKolicina", listaKolicina);
		model.addAttribute("size",listaProizvoda.size());
		return "meduskladisnice/detaljiMeduskladisnica";
		
	}
	
	@RequestMapping(value="/meduskladisnica/promijeni")
	public String prikaziIzbornik(Model model){
		
		List<Skladiste> pocetnaSkladista=skladisteService.fetchAllSkladiste();
		List<Skladiste> odredisnaSkladista=skladisteService.fetchAllSkladiste();
		
		
		model.addAttribute("pocetnaSkladista", pocetnaSkladista);
		model.addAttribute("odredisnaSkladista", odredisnaSkladista);
		
		
		
		return "meduskladisnice/promijeniSkladiste";
		
	}


	@RequestMapping(value="/meduskladisnica/prikaziProizvode",method=RequestMethod.POST)
	public  String  prikaziProizvode(@RequestParam("sourceSkladisteId") Integer sourceSkladisteId,@RequestParam("targetSkladisteId") Integer targetSkladisteId,@RequestParam("sifra") String sifra ,Model model) 
	{
	  

		List<ProizvodPrenesiDto> listaProizvodPrenesiDto=new ArrayList<>();
		
		
		
	 
	  List<SkladisteProizvod> listaSkladisteProizvod=skladisteProizvodService.fetchAllSkladisteProizvod();
			 
      for (SkladisteProizvod skladisteProizvod:listaSkladisteProizvod){
    	  
    	  if (skladisteProizvod.getSkladisteId()==sourceSkladisteId){
    		  ProizvodPrenesiDto prenesi=new ProizvodPrenesiDto();
    		  prenesi.setProizvodId(skladisteProizvod.getProizvodId());
    		  prenesi.setKolicina(skladisteProizvod.getKolicina());
    		  prenesi.setSourceSkladisteId(skladisteProizvod.getSkladisteId());
    		  prenesi.setTargetSkladisteId(targetSkladisteId);
    		  prenesi.setMeduskladisnicaSifra(sifra);
    		  prenesi.setPrenesiKolicinu(0);
    		  prenesi.setMeduskladisnicaId(null);
    		  prenesi.setIdentifikator(skladisteProizvod.getId());
    		Proizvod proizvod=  proizvodService.findProizvodById(skladisteProizvod.getProizvodId());
    		VrstaProizvoda vrsta=  vrstaProizvodaService.fetchVrstaProizvodaById(proizvod.getId());
    		  prenesi.setNaziv(vrsta.getNaziv());
    		  
    		  
    		  listaProizvodPrenesiDto.add(prenesi);
    		  
    		  
    		  
    		
              
            
              
    	  }
    	  
      }
      ProizvodPrenesiDtoWrapper wrapper=new ProizvodPrenesiDtoWrapper();
      wrapper.setProizvodiList((ArrayList<ProizvodPrenesiDto>)listaProizvodPrenesiDto);
      model.addAttribute("wrapper",wrapper );
   return "meduskladisnice/proizvodiZaPrijenos";
		 
	 }
	@RequestMapping(value="/meduskladisnica/prenesiIPreuredi",method=RequestMethod.POST)
	public String napraviPrijenos(@Valid @ModelAttribute("wrapper") final ProizvodPrenesiDtoWrapper wrapper,Model model){
		List<ProizvodPrenesiDto> prenesi=wrapper.getProizvodiList();
		
		
		
		Meduskladisnica meduskladisnica=new Meduskladisnica();
		meduskladisnica.setSifra(wrapper.getProizvodiList().get(0).getMeduskladisnicaSifra());
		meduskladisnica.setSourceSkladiste(skladisteService.fetchSkladisteById(wrapper.getProizvodiList().get(0).getSourceSkladisteId()));
		meduskladisnica.setTargetSkladiste(skladisteService.fetchSkladisteById(wrapper.getProizvodiList().get(0).getTargetSkladisteId()));
		meduskladisnica.setDatumKreiranja(new Date());
		meduskladisnicaService.saveMeduskladisnica(meduskladisnica);
		

		List<ProizvodPrenesiDto> proizvodiDto=wrapper.getProizvodiList();
		
		
		
		
	
		
		for(ProizvodPrenesiDto proizvodDto:proizvodiDto){
			
			if (proizvodDto.getPrenesiKolicinu()>0){
				MeduskladisnicaProizvod meduskladisnicaProizvod=new MeduskladisnicaProizvod();	
				meduskladisnicaProizvod.setDatumKreiranja(new Date());
				meduskladisnicaProizvod.setKolicina(proizvodDto.getPrenesiKolicinu());
				//QueryDSL 
				meduskladisnicaProizvod.setMeduskladisnica(meduskladisnicaService.findMeduskladisnicaBySifra(proizvodDto.getMeduskladisnicaSifra()));
	            meduskladisnicaProizvod.setProizvod(proizvodService.findProizvodById(proizvodDto.getProizvodId()));		
	        	mpService.saveMeduskladisnicaProizvod(meduskladisnicaProizvod);		
			}
		
			
		}
	
		for(ProizvodPrenesiDto proizvodDto:proizvodiDto){
			
		
				
				Integer novaKolicina=proizvodDto.getKolicina()-proizvodDto.getPrenesiKolicinu();
	
				//QueryDSL
				
			SkladisteProizvod	skladisteProizvodZaSource=skladisteProizvodService.findSkladisteProizvodByProizvodIdAndSkladisteIdAndId(proizvodDto.getProizvodId(),proizvodDto.getSourceSkladisteId(),proizvodDto.getIdentifikator());
			skladisteProizvodZaSource.setDatumIzmjene(new Date());
			skladisteProizvodZaSource.setProizvodId(proizvodDto.getProizvodId());
			skladisteProizvodZaSource.setKolicina(novaKolicina);
			skladisteProizvodService.saveSkladisteProizvod(skladisteProizvodZaSource);
		     
			
		/*	
			SkladisteProizvod skladisteProizvodZaTarget=new SkladisteProizvod();
			skladisteProizvodZaTarget.setDatumKreiranja(new Date());
			skladisteProizvodZaTarget.setProizvodId(proizvodDto.getProizvodId());
			skladisteProizvodZaTarget.setKolicina(proizvodDto.getPrenesiKolicinu());
			skladisteProizvodZaTarget.setSkladisteId(proizvodDto.getTargetSkladisteId());
	        skladisteProizvodService.saveSkladisteProizvod(skladisteProizvodZaTarget);
		*/				
			}
			
			
			
			
			
			
			
		
			
			
		
		
		
		
		
		
		return "redirect:/meduskladisnice/lista";
		

		
	}

}

