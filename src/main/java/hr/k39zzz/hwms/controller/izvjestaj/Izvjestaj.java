package hr.k39zzz.hwms.controller.izvjestaj;

public class Izvjestaj {

	private String mjesec;

	private Long brojacPrimka;

	private Long brojacOtprema;

	public String getMjesec() {
		return mjesec;
	}

	public void setMjesec(final String mjesec) {
		this.mjesec = mjesec;
	}

	public Long getBrojacPrimka() {
		return brojacPrimka;
	}

	public void setBrojacPrimka(Long brojacPrimka) {
		this.brojacPrimka = brojacPrimka;
	}

	public Long getBrojacOtprema() {
		return brojacOtprema;
	}

	public void setBrojacOtprema(Long brojacOtprema) {
		this.brojacOtprema = brojacOtprema;
	}

}
