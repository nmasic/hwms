package hr.k39zzz.hwms.controller.izvjestaj;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.k39zzz.hwms.service.OtpremnicaProizvodService;
import hr.k39zzz.hwms.service.PrimkaProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class IzvjestajPrometController {

	@Autowired
	private PrimkaProizvodService primkaProizvodService;

	@Autowired
	private SkladisteService skladisteService;

	@Autowired
	private OtpremnicaProizvodService otpremnicaProizvodService;

	@RequestMapping(value = "/izvjestaj/promet", method = RequestMethod.GET)
	public String getPocetnaStranica(final Model model) {
		model.addAttribute("skladista", skladisteService.fetchAllSkladiste());
		return "izvjestaji/izvjestajPromet";
	}

	@RequestMapping(value = "/izvjestaj/promet/aktivnosti", method = RequestMethod.GET)
	@ResponseBody
	public List<Izvjestaj> getPodaci(@RequestParam(required = false) final Integer godina,
			@RequestParam(required = false) final Integer skladiste) {

		int godinaQuery;
		if (godina == null) {
			godinaQuery = GregorianCalendar.getInstance().get(Calendar.YEAR);
		} else {
			godinaQuery = godina;
		}

		List<Object[]> primkaPoMjesecima = primkaProizvodService.sumPrimkaPerMonthByYearAndSkladiste(godinaQuery,
				skladiste);
		final Map<String, Long> monthsMapPrimke = napraviMapuSvihMjeseca(primkaPoMjesecima);

		List<Object[]> otpremnicaPoMjesecima = otpremnicaProizvodService
				.sumOtpremnicaPerMonthByYearAndSkladiste(godinaQuery, skladiste);
		final Map<String, Long> monthsMapOtpremnice = napraviMapuSvihMjeseca(otpremnicaPoMjesecima);

		final List<Izvjestaj> izvjestaji = new ArrayList<>();
		for (final Entry<String, Long> entry : monthsMapPrimke.entrySet()) {
			final Izvjestaj izvjestaj = new Izvjestaj();
			izvjestaj.setMjesec(entry.getKey());
			izvjestaj.setBrojacPrimka(entry.getValue());
			izvjestaj.setBrojacOtprema(monthsMapOtpremnice.get(entry.getKey()));
			izvjestaji.add(izvjestaj);
		}

		return izvjestaji;
	}

	private Map<String, Long> napraviMapuSvihMjeseca(List<Object[]> podaciPoMjesecima) {

		final Map<String, Long> monthsMap = new LinkedHashMap<>();
		final String[] months = new DateFormatSymbols().getMonths();
		for (int i = 0; i < months.length; i++) {
			monthsMap.put(months[i], Long.valueOf(0));
		}

		for (final Object[] objects : podaciPoMjesecima) {
			final String mjesecString = getMonth((Integer) objects[1]);
			monthsMap.put(mjesecString, (Long) objects[0]);
		}
		return monthsMap;
	}

	public String getMonth(final int month) {
		return new DateFormatSymbols().getMonths()[month - 1];
	}
}
