package hr.k39zzz.hwms.controller.korisnik;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import hr.k39zzz.hwms.model.KorisnikRola;

public class KorisnikRolaFormatter implements Formatter<KorisnikRola> {

	@Override
	public String print(KorisnikRola rola, Locale locale) {
		return rola.getRola();
	}

	@Override
	public KorisnikRola parse(String id, Locale locale) throws ParseException {
		KorisnikRola rola = new KorisnikRola();
		rola.setRola(id);
		return rola;
	}
}
