package hr.k39zzz.hwms.controller.korisnik;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import hr.k39zzz.hwms.service.KorisnikService;

@Controller
public class KorisnikListaController {

	@Autowired
	private KorisnikService korisnikService;

	@RequestMapping("/admin/korisnik/lista")
	public String korisnikLista(final Model model) {
		model.addAttribute("korisnici", korisnikService.findAll());
		return "korisnik/korisnikLista";
	}
}
