package hr.k39zzz.hwms.controller.korisnik;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import hr.k39zzz.hwms.model.Korisnik;
import hr.k39zzz.hwms.service.KorisnikService;

@Controller
public class LozinkaController {

	@Autowired
	private KorisnikService korisnikService;

	@RequestMapping(value = "/korisnik/lozinka", method = RequestMethod.GET)
	public String getForm(Model model) {
		model.addAttribute("lozinkaForm", new LozinkaForm());
		return "korisnik/lozinkaForm";
	}

	@RequestMapping(value = "/korisnik/lozinka", params = "odustani", method = RequestMethod.POST)
	public String promjenaLozinkeOdustani() {
		return "redirect:/pocetna";
	}

	@RequestMapping(value = "/korisnik/lozinka", params = "potvrdi", method = RequestMethod.POST)
	public String promjenaLozinkePotvrdi(@Valid @ModelAttribute("lozinkaForm") final LozinkaForm lozinkaForm,
			BindingResult bindingResult, Principal principal) {

		if (bindingResult.hasErrors()) {
			return "korisnik/lozinkaForm";
		}

		if (!lozinkaForm.getNovaLozinka().equals(lozinkaForm.getNovaLozinkaPotvrda())) {
			bindingResult.rejectValue("novaLozinkaPotvrda", "korisnik.novaLozinka.ista");
			return "korisnik/lozinkaForm";
		}

		Korisnik korisnik = korisnikService.findByKorisnickoIme(principal.getName());

		if (korisnikService.lozinkeNeOdgovaraju(lozinkaForm.getTrenutnaLozinka(), korisnik.getLozinka())) {
			bindingResult.rejectValue("trenutnaLozinka", "korisnik.staraLozinka.kriva");
			return "korisnik/lozinkaForm";
		}

		// TODO: dogovoriti da li trebaju kontrole na slozenost lozinke

		korisnikService.updateLozinka(lozinkaForm.getNovaLozinka(), principal.getName());

		return "redirect:/korisnik/lozinka/uspjesno";
	}

	@RequestMapping(value = "/korisnik/lozinka/uspjesno", method = RequestMethod.GET)
	public String promjenaLozinkeUspjesno() {
		return "korisnik/lozinkaUspjesno";
	}

}
