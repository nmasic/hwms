package hr.k39zzz.hwms.controller.korisnik;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class LozinkaForm {

	@NotEmpty
	@Size(min = 3, max = 64)
	private String trenutnaLozinka;
	
	@NotEmpty
	@Size(min = 3, max = 64)
	private String novaLozinka;
	
	@NotEmpty
	@Size(min = 3, max = 64)
	private String novaLozinkaPotvrda;

	
	public String getNovaLozinka() {
		return novaLozinka;
	}

	public void setNovaLozinka(String novaLozinka) {
		this.novaLozinka = novaLozinka;
	}

	public String getNovaLozinkaPotvrda() {
		return novaLozinkaPotvrda;
	}

	public void setNovaLozinkaPotvrda(String novaLozinkaPotvrda) {
		this.novaLozinkaPotvrda = novaLozinkaPotvrda;
	}

	public String getTrenutnaLozinka() {
		return trenutnaLozinka;
	}

	public void setTrenutnaLozinka(String trenutnaLozinka) {
		this.trenutnaLozinka = trenutnaLozinka;
	}

}
