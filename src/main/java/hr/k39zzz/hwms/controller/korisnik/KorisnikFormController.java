package hr.k39zzz.hwms.controller.korisnik;

import java.security.Principal;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.k39zzz.hwms.model.Korisnik;
import hr.k39zzz.hwms.model.Rola;
import hr.k39zzz.hwms.service.KorisnikService;

@Controller
@RequestMapping("/admin/korisnik/unos")
public class KorisnikFormController {

	private static final String FORM_VIEW = "korisnik/korisnikForm";

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private MessageSource messageSource;

	@ModelAttribute("sveRole")
	public Rola[] getSveRole() {
		return Rola.values();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String obradiGet(@RequestParam(required = false) Integer id, final Model model, Locale locale) {
		if (id != null) {
			model.addAttribute("korisnikForm", korisnikService.getById(id));
			model.addAttribute("naslov", messageSource.getMessage("korisnik.promjena.naslov", null, locale));
		} else {
			Korisnik korisnik = new Korisnik();
			korisnik.setAktivan(true);
			model.addAttribute("korisnikForm", korisnik);
			model.addAttribute("naslov", messageSource.getMessage("korisnik.unos.naslov", null, locale));
		}
		return FORM_VIEW;
	}

	@RequestMapping(params = "odustani", method = RequestMethod.POST)
	public String odustaniForm() {
		return "redirect:/admin";
	}

	@RequestMapping(params = "potvrdi", method = RequestMethod.POST)
	public String potvrdiForm(@Valid @ModelAttribute("korisnikForm") final Korisnik korisnik,
			BindingResult bindingResult, Principal principal, Locale locale, RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			return FORM_VIEW;
		}

		if (!validateKorisnik(korisnik, bindingResult)) {
			return FORM_VIEW;
		}

		String codePoruka = null;
		if (korisnik.getId() != null) {
			korisnik.setKorisnikIzmjene(principal.getName());
			codePoruka = "korisnik.promjena.uspjesno";
		} else {
			korisnik.setKorisnikKreiranja(principal.getName());
			codePoruka = "korisnik.unos.uspjesno";
		}

		korisnikService.saveKorisnik(korisnik);

		redirectAttributes.addFlashAttribute("poruka",
				messageSource.getMessage(codePoruka, new Object[] { korisnik.getPunoIme() }, locale));

		return "redirect:/admin/korisnik/lista";
	}

	private boolean validateKorisnik(final Korisnik korisnik, BindingResult bindingResult) {
		if (korisnik.getRole() == null || korisnik.getRole().isEmpty() || korisnik.getRole().size() > 1) {
			bindingResult.rejectValue("role", "korisnik.role.obavezno");
			return false;
		}

		if (korisnik.getKorisnickoIme() == null) {
			bindingResult.rejectValue("korisnickoIme", "korisnik.korisnickoIme.obavezno");
			return false;
		}

		if (korisnik.getId() == null && (korisnik.getLozinka() == null || korisnik.getLozinka().isEmpty())) {
			bindingResult.rejectValue("lozinka", "korisnik.lozinka.obavezno");
			return false;
		}

		if (korisnik.getId() == null && korisnikService.postojiByKorisnickoIme(korisnik.getKorisnickoIme())) {
			bindingResult.reject("korisnik.unos.vecPostoji");
			return false;

		}

		return true;
	}

}
