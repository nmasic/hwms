package hr.k39zzz.hwms.controller.skladiste.stanje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class KolicinskoStanjeController {

	@Autowired
	private SkladisteService skladisteService;
	
	@Autowired
	private ProizvodService proizvodService;
	
	@RequestMapping(value = "/skladiste/stanje/kolicinsko/skladista", method = RequestMethod.GET)
	public String getListaSkladiste(final Model model) {
		model.addAttribute("svaSkladista", skladisteService.fetchAllSkladiste());
		return "skladiste/stanje/kolicinskoStanje";
	}

	@RequestMapping(value = "/skladiste/stanje/kolicinsko/proizvodi", method = RequestMethod.GET)
	public String getProizvodiSkladista(@RequestParam("skid") int skladisteId, final Model model) {

		model.addAttribute("svaSkladista", skladisteService.fetchAllSkladiste());
		model.addAttribute("proizvodList", proizvodService.findProizvodStanjeBySkladisteId(skladisteId));

		return "skladiste/stanje/kolicinskoStanje";
	}
	
	@RequestMapping(value = "/skladiste/stanje/kolicinsko/proizvod", method = RequestMethod.GET)
	public String getProizvodDokumenti(@RequestParam("skid") int skladisteId, @RequestParam("pid") int proizvodId, Model model) {
		
		model.addAttribute("dokumenti", proizvodService.sviDokumentZaProizvodSkladiste(skladisteId, proizvodId));
		model.addAttribute("skladiste", skladisteService.fetchSkladisteById(skladisteId));
		model.addAttribute("proizvod", proizvodService.findProizvodById(proizvodId));
		
		return "skladiste/stanje/kolicinskoStanjeProizvod";
		
	}

}
