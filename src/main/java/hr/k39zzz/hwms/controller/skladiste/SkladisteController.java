package hr.k39zzz.hwms.controller.skladiste;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.SkladisteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;


@Controller
public class SkladisteController {
	
	@Autowired
    private SkladisteService skladisteService;

    private static final Logger LOG = LoggerFactory.getLogger(SkladisteController.class);

	@RequestMapping(value = "/skladiste/index", method = RequestMethod.GET)
    public String getListaSkladiste(final Model model) {
	    LOG.info("Skladiste - begin action getListaSkladiste()");
        List<Skladiste> skladisteList = new ArrayList<>();
        skladisteList = skladisteService.fetchAllSkladiste();
        model.addAttribute("skladisteList", skladisteList);
        return "skladiste/skladisteLista";
    }

    @RequestMapping(value = "/skladiste/proizvodi", method = RequestMethod.GET)
    public String getProizvodiSkladista(@RequestParam("id") long id, final Model model) {
        LOG.info("Skladiste - begin action getProizvodiSkladista({})", id);
        List<Proizvod> proizvodList = skladisteService.findAllProizvodiForSkladisteId((int)id);
        Skladiste skladiste = skladisteService.fetchSkladisteById((int)id);
        model.addAttribute("proizvodList", proizvodList);
        model.addAttribute("skladiste", skladiste);
        return "skladiste/proizvodiSkladista";
    }
	
	
	/*@RequestMapping(value = "/skladiste/unos", method = RequestMethod.GET)
    public String getUnosSkladiste() {
		
        return "skladiste/skladisteUnos";
    }
	*/
}