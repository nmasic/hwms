package hr.k39zzz.hwms.controller.skladiste;

import java.util.Date;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class SkladisteUnosController {
	@Autowired
	private SkladisteService skladisteService;
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/skladiste/unos", method = RequestMethod.GET)
	public String getForm(@RequestParam(required = false) Integer id, final Model model, Locale locale) {

		if (id == null) {
			model.addAttribute("skladisteUnos", new Skladiste());
			model.addAttribute("naslov", messageSource.getMessage("skladiste.unos.naslov", null, locale));
		} else {
			model.addAttribute("skladisteUnos", skladisteService.fetchSkladisteById(id));
			model.addAttribute("naslov", messageSource.getMessage("skladiste.promjena.naslov", null, locale));
		}

		return "skladiste/skladisteUnos";
	}

	@RequestMapping(value = "/skladiste/unos", params = "odustani", method = RequestMethod.POST)
	public String odustaniFormProizvod() {
		return "redirect:/skladiste/index";
	}

	@RequestMapping(value = "/skladiste/unos", params = "spremi", method = RequestMethod.POST)
	public String potvrdiFormProizvod(@Valid @ModelAttribute("skladisteUnos") final Skladiste skladiste,
			BindingResult bindingResult, Locale locale, RedirectAttributes redirectAttributes,
			@RequestParam(required = false) Integer id) {

		if (bindingResult.hasErrors()) {

			return "skladiste/skladisteUnos";
		}

		if (!validateSkladiste(skladiste, bindingResult)) {
			return "skladiste/skladisteUnos";
		}

		String codePoruka = null;
		if (skladiste.getId() != null) {

			skladisteService.saveSkladiste(skladiste);
			codePoruka = "skladiste.promjena.uspjesno";

		} else {

			// skladiste sa hidden id-jem je promijenjeno

			if (id != null) {
				Skladiste s = skladisteService.fetchSkladisteById(id);
				s.setNaziv(skladiste.getNaziv());
				s.setSifra(skladiste.getSifra());
				s.setKapacitet(skladiste.getKapacitet());
				s.setId(id);
				s.setDatumIzmjene(new Date());
				skladisteService.saveSkladiste(s);
				codePoruka = "skladiste.promijena.uspjesno";
				redirectAttributes.addFlashAttribute("poruka",
						messageSource.getMessage(codePoruka, new Object[] {}, locale));

				return "redirect:/skladiste/index";
			}

			skladisteService.saveSkladiste(skladiste);
			codePoruka = "skladiste.unos.uspjesno";
		}

		redirectAttributes.addFlashAttribute("poruka", messageSource.getMessage(codePoruka, new Object[] {}, locale));

		return "redirect:/skladiste/index";
	}

	private boolean validateSkladiste(final Skladiste skladiste, BindingResult bindingResult) {
		if (skladiste.getNaziv() == null || skladiste.getNaziv().isEmpty()) {
			bindingResult.rejectValue("naziv", "proizvod.naziv.obavezno");
			return false;
		}

		if (skladiste.getSifra() == null || skladiste.getSifra().isEmpty()) {
			bindingResult.rejectValue("sifra", "skladiste.sifra.obavezno");
			return false;
		}
		if (skladiste.getKapacitet() == null) {
			bindingResult.rejectValue("kapacitet", "skladiste.kapacitet.obavezno");
			return false;
		}

		return true;
	}

}
