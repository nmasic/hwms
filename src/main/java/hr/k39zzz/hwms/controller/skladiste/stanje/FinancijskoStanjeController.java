package hr.k39zzz.hwms.controller.skladiste.stanje;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.ProizvodService;
import hr.k39zzz.hwms.service.SkladisteService;

@Controller
public class FinancijskoStanjeController {

	@Autowired
	private SkladisteService skladisteService;

	@Autowired
	private ProizvodService proizvodService;

	@RequestMapping(value = "/financijskoStanje", method = RequestMethod.GET)
	public String getListaSkladiste(final Model model) {
		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();
		List<Skladiste> listaSkladista = skladisteService.fetchAllSkladiste();

		model.addAttribute("svaSkladista", listaSkladista);
		model.addAttribute("sviProizvodi", listaProizvoda);
		model.addAttribute("ukupnaCijena", getUkupnaCijenaPoProizvodu(listaProizvoda));
		model.addAttribute("cijenaSvega", getUkupnaCijenaPoSkladistu(listaSkladista));

		return "izvjestaji/financijskoStanje";
	}

	public BigDecimal getUkupnaCijenaPoProizvodu(List<Proizvod> listaProizvoda) {
		BigDecimal ukupnaCijena = new BigDecimal(0);
		for (Proizvod proizvod : listaProizvoda) {
			ukupnaCijena = ukupnaCijena.add(proizvod.getUkupnaCijenaOvogProizvodaUsvimSkladistima());
		}

		return ukupnaCijena;
	}

	public BigDecimal getUkupnaCijenaPoSkladistu(List<Skladiste> listaSkladista) {
		BigDecimal ukupnaCijena = new BigDecimal(0);
		for (Skladiste skladiste : listaSkladista) {
			ukupnaCijena = ukupnaCijena.add(skladiste.getUkupnaCijenaSvihProizvodaUSkladistu());
		}

		return ukupnaCijena;
	}

	@RequestMapping(value = "/financijskoStanje/proizvodi", method = RequestMethod.GET)
	public String getFinancijskoStanjePoProizvodu(final Model model) {

		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();
		List<Skladiste> listaSkladista = skladisteService.fetchAllSkladiste();

		model.addAttribute("svaSkladista", listaSkladista);
		model.addAttribute("sviProizvodi", listaProizvoda);
		model.addAttribute("ukupnaCijena", getUkupnaCijenaPoProizvodu(listaProizvoda));
		model.addAttribute("cijenaSvega", getUkupnaCijenaPoSkladistu(listaSkladista));

		model.addAttribute("proizvodList", proizvodService.fetchAllProizvod());

		return "izvjestaji/financijskoStanje";
	}

	@RequestMapping(value = "/financijskoStanje/skladista", method = RequestMethod.GET)
	public String getFinancijskoStanjePoSkladistu(final Model model) {

		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();
		List<Skladiste> listaSkladista = skladisteService.fetchAllSkladiste();

		model.addAttribute("svaSkladista", listaSkladista);
		model.addAttribute("sviProizvodi", listaProizvoda);
		model.addAttribute("ukupnaCijena", getUkupnaCijenaPoProizvodu(listaProizvoda));
		model.addAttribute("cijenaSvega", getUkupnaCijenaPoSkladistu(listaSkladista));

		model.addAttribute("skladisteList", skladisteService.fetchAllSkladiste());

		return "izvjestaji/financijskoStanje";
	}

	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletResponse response) throws IOException {

		response.setContentType("text/csv");
		String reportName = "CSV_Report_Name.csv";
		response.setHeader("Content-disposition", "attachment;filename=" + reportName);

		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();

		ArrayList<String> rows = new ArrayList<>();
		rows.add("Name,Result");
		rows.add("\n");

		for (Proizvod proizvod : listaProizvoda) {
			rows.add(proizvod.getNaziv() + "," + proizvod.getCijena() + "\n");
		}

		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}

		response.getOutputStream().flush();

	}

	@RequestMapping(value = "/downloadCSV2")
	public void downloadCSV2(HttpServletResponse response) throws IOException {

		response.setContentType("text/csv");
		String reportName = "CSV_Report_Name.csv";
		response.setHeader("Content-disposition", "attachment;filename=" + reportName);

		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();

		// uses the Super CSV API to generate CSV data from the model data
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "naziv", "opis", "cijena" };

		csvWriter.writeHeader(header);

		for (Proizvod proizvod : listaProizvoda) {
			csvWriter.write(proizvod, header);
		}

		csvWriter.close();
	}

	@RequestMapping(value = "/downloadCSV3")
	public ModelAndView viewCSV() throws IOException {

		List<Proizvod> listaProizvoda = proizvodService.fetchAllProizvod();

		String[] header = { "naziv", "opis", "cijena" };

		ModelAndView model = new ModelAndView("ViewCSV");
		model.addObject("csvData", listaProizvoda);
		model.addObject("csvHeader", header);

		return model;
	}

}
