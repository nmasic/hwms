package hr.k39zzz.hwms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DatumKreiranja", insertable = true, updatable = false)
	private Date datumKreiranja;

	@Column(name = "KorisnikKreiranja", insertable = true, updatable = false)
	private String korisnikKreiranja;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DatumIzmjene", insertable = false, updatable = true)
	private Date datumIzmjene;

	@Column(name = "KorisnikIzmjene", insertable = false, updatable = true)
	private String korisnikIzmjene;

	@PrePersist
	protected void onCreate() {
		datumKreiranja = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		datumIzmjene = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Date getDatumIzmjene() {
		return datumIzmjene;
	}

	public void setDatumIzmjene(Date datumIzmjene) {
		this.datumIzmjene = datumIzmjene;
	}

	public String getKorisnikIzmjene() {
		return korisnikIzmjene;
	}

	public void setKorisnikIzmjene(String korisnikIzmjene) {
		this.korisnikIzmjene = korisnikIzmjene;
	}

	public String getKorisnikKreiranja() {
		return korisnikKreiranja;
	}

	public void setKorisnikKreiranja(String korisnikKreiranja) {
		this.korisnikKreiranja = korisnikKreiranja;
	}

}
