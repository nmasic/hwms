package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Proizvod extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotEmpty
	@Size(min = 2, max = 255)
	@Column(name = "Naziv")
	private String naziv;

	@Size(max = 255)
	@Column(name = "Opis")
	private String opis;

	@NotNull
	@Column(name = "Cijena")
	private BigDecimal cijena;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VrstaProizvodaId")
	private VrstaProizvoda vrstaProizvoda;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proizvod")
	private Set<PrimkaProizvod> primkaProizvod;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proizvod")
	private List<SkladisteProizvod> skladisteProizvod;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proizvod")
	private Set<MeduskladisnicaProizvod> meduskladisnicaProizvod;

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public BigDecimal getCijena() {
		return cijena;
	}

	public void setCijena(BigDecimal cijena) {
		this.cijena = cijena;
	}

	public VrstaProizvoda getVrstaProizvoda() {
		return vrstaProizvoda;
	}

	public void setVrstaProizvoda(VrstaProizvoda vrstaProizvoda) {
		this.vrstaProizvoda = vrstaProizvoda;
	}

	public Set<PrimkaProizvod> getPrimkaProizvod() {
		return primkaProizvod;
	}

	public void setPrimkaProizvod(Set<PrimkaProizvod> primkaProizvod) {
		this.primkaProizvod = primkaProizvod;
	}

	public List<SkladisteProizvod> getSkladisteProizvod() {
		return skladisteProizvod;
	}

	public void setSkladisteProizvod(List<SkladisteProizvod> skladisteProizvod) {
		this.skladisteProizvod = skladisteProizvod;
	}
	
	public BigDecimal getUkupnaCijenaOvogProizvodaUsvimSkladistima() {
		BigDecimal ukupnaCijena = new BigDecimal(0);
		
		Iterator<SkladisteProizvod> iter = this.skladisteProizvod.iterator();
        while (iter.hasNext()){
        	SkladisteProizvod skladisteProizvoda = iter.next();
        	ukupnaCijena = ukupnaCijena.add(cijena.multiply(new BigDecimal(skladisteProizvoda.getKolicina())));
        	
        }
		return ukupnaCijena;
	}
	
	public Integer getUkupnaKolicinaUsvimSkladistima() {
		//Integer ukupnaKolicina = new Integer(0);
		 
		
		/*Iterator<SkladisteProizvod> iter = this.skladisteProizvod.iterator();
        while (iter.hasNext()){
        	SkladisteProizvod skladisteProizvoda = iter.next();
        	ukupnaKolicina = ukupnaKolicina + skladisteProizvoda.getKolicina();
        	
        }*/
		Integer ukupnaKolicina = skladisteProizvod.stream().mapToInt(s -> s.getKolicina()).sum();
		
		return ukupnaKolicina;
	}
}
