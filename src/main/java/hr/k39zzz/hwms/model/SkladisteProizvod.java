package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class SkladisteProizvod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "SkladisteId", insertable = false, updatable = false)
	private Integer skladisteId;

	@NotNull
	@Column(name = "ProizvodId", insertable = false, updatable = false)
	private Integer proizvodId;

	@Column(name = "Kolicina")
	private Integer kolicina;

	@NotNull
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@Column(name = "DatumIzmjene")
	private Date datumIzmjene;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "SkladisteId")
	private Skladiste skladiste;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "ProizvodId")
	private Proizvod proizvod;

	public SkladisteProizvod() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkladisteId() {
		return skladisteId;
	}

	public void setSkladisteId(Integer skladisteId) {
		this.skladisteId = skladisteId;
	}

	public Integer getProizvodId() {
		return proizvodId;
	}

	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Date getDatumIzmjene() {
		return datumIzmjene;
	}

	public void setDatumIzmjene(Date datumIzmjene) {
		this.datumIzmjene = datumIzmjene;
	}

	public Skladiste getSkladiste() {
		return skladiste;
	}

	public void setSkladiste(Skladiste skladiste) {
		this.skladiste = skladiste;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

}
