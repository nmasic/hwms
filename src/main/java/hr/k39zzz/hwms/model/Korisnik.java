package hr.k39zzz.hwms.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "Korisnik")
public class Korisnik extends AbstractEntity implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Size(min = 2, max = 100)
	@Column(name = "KorisnickoIme", updatable = false)
	private String korisnickoIme;

	@Column(name = "Lozinka", updatable = false)
	private String lozinka;

	@NotEmpty
	@Size(min = 2, max = 30)
	@Column(name = "Ime")
	private String ime;

	@NotEmpty
	@Size(min = 2, max = 50)
	@Column(name = "Prezime")
	private String prezime;

	@NotEmpty
	@Size(min = 2, max = 200)
	@Column(name = "Email")
	private String email;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(name = "Aktivan")
	private boolean aktivan;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "korisnik", fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<KorisnikRola> role = new HashSet<>();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return role;
	}

	@Override
	public String getPassword() {
		return lozinka;
	}

	@Override
	public String getUsername() {
		return korisnickoIme;
	}

	@Override
	public boolean isAccountNonExpired() {
		return aktivan;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<KorisnikRola> getRole() {
		return role;
	}

	public void setRole(Set<KorisnikRola> role) {
		this.role = role;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	public String getPunoIme() {
		return this.ime + " " + this.prezime;
	}

	public String getSveRole() {
		if (role != null) {
			return role.stream().map(Object::toString).collect(Collectors.joining(", "));
		}
		return "";
	}

}
