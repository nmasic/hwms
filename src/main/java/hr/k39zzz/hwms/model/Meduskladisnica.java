package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
public class Meduskladisnica implements Serializable {
	
	  
	
	public Meduskladisnica(){}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	@Size(min = 5, max = 50)
	@Column(name = "Sifra")
	private String sifra;
	
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SourceSkladisteId")
	private Skladiste sourceSkladiste;
	
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TargetSkladisteId")
	private Skladiste targetSkladiste;
	
	
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meduskladisnica")
	private Set<MeduskladisnicaProizvod> meduskladisnicaProizvod;



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSifra() {
		return sifra;
	}


	public void setSifra(String sifra) {
		this.sifra = sifra;
	}


	public Skladiste getSourceSkladiste() {
		return sourceSkladiste;
	}


	public void setSourceSkladiste(Skladiste sourceSkladiste) {
		this.sourceSkladiste = sourceSkladiste;
	}


	public Skladiste getTargetSkladiste() {
		return targetSkladiste;
	}


	public void setTargetSkladiste(Skladiste targetSkladiste) {
		this.targetSkladiste = targetSkladiste;
	}


	public Date getDatumKreiranja() {
		return datumKreiranja;
	}


	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

}
