package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Skladiste implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@NotEmpty
	@Size(min = 5, max = 50)
	@Column(name = "Sifra")
	private String sifra;

	@NotEmpty
	@Size(min = 2, max = 255)
	@Column(name = "Naziv")
	private String naziv;

	@DateTimeFormat(pattern = "dd-MMM-YYYY")
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@Column(name = "DatumIzmjene")
	private Date datumIzmjene;

	@NotNull
	@Column(name = "Kapacitet")
	private Integer kapacitet;

	@OneToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST})
	@JoinColumn(name="SkladisteId")
	private List<SkladisteProizvod> skladisteProizvod;
	
	@OneToMany(mappedBy="sourceSkladiste",fetch = FetchType.LAZY)
	private Set<Meduskladisnica> meduskladisniceSourceSkladiste;
	
	@OneToMany(mappedBy="targetSkladiste",fetch = FetchType.LAZY)
	private Set<Meduskladisnica> meduskladisniceTargetSkladiste;

	public Integer getId() {
		return id;
	}

	public Set<Meduskladisnica> getMeduskladisniceSourceSkladiste() {
		return meduskladisniceSourceSkladiste;
	}

	public void setMeduskladisniceSourceSkladiste(Set<Meduskladisnica> meduskladisniceSourceSkladiste) {
		this.meduskladisniceSourceSkladiste = meduskladisniceSourceSkladiste;
	}

	public Set<Meduskladisnica> getMeduskladisniceTargetSkladiste() {
		return meduskladisniceTargetSkladiste;
	}

	public void setMeduskladisniceTargetSkladiste(Set<Meduskladisnica> meduskladisniceTargetSkladiste) {
		this.meduskladisniceTargetSkladiste = meduskladisniceTargetSkladiste;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Date getDatumIzmjene() {
		return datumIzmjene;
	}

	public void setDatumIzmjene(Date datumIzmjene) {
		this.datumIzmjene = datumIzmjene;
	}

	public Integer getKapacitet() {
		return kapacitet;
	}

	public void setKapacitet(Integer kapacitet) {
		this.kapacitet = kapacitet;
	}

	public List<SkladisteProizvod> getSkladisteProizvod() {
		return skladisteProizvod;
	}

	public void setSkladisteProizvod(List<SkladisteProizvod> skladisteProizvod) {
		this.skladisteProizvod = skladisteProizvod;
	}
	
	public BigDecimal getUkupnaCijenaSvihProizvodaUSkladistu(){
		BigDecimal ukupnaCijena = new BigDecimal(0);
		
		Iterator<SkladisteProizvod> iter = this.skladisteProizvod.iterator();
        while (iter.hasNext()){
        	SkladisteProizvod skladisteProizvoda = iter.next();
        	ukupnaCijena = ukupnaCijena.add(skladisteProizvoda.getProizvod().getCijena().multiply(new BigDecimal(skladisteProizvoda.getKolicina())));
        	
        }
		return ukupnaCijena;
	}
	
	public Integer getUkupnaKolicinaProizvodaUovomSkladistu() {
		Integer ukupnaKolicina = new Integer(0);
		
		Iterator<SkladisteProizvod> iter = this.skladisteProizvod.iterator();
        while (iter.hasNext()){
        	SkladisteProizvod skladisteProizvoda = iter.next();
        	ukupnaKolicina = ukupnaKolicina + skladisteProizvoda.getKolicina();
        	
        }
		return ukupnaKolicina;
	}
}
