package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class OtpremnicaProizvod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "ProizvodId", insertable = false, updatable = false)
	private Integer proizvodId;

	@NotNull
	@Column(name = "OtpremnicaId", insertable = false, updatable = false)
	private Integer otpremnicaId;

	@Column(name = "Kolicina")
	@Min(1)
	@Max(1000)
	private Integer kolicina;

	@NotNull
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ProizvodId")
	private Proizvod proizvod;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "OtpremnicaId")
	private Otpremnica otpremnica;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProizvodId() {
		return proizvodId;
	}

	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}

	public Integer getOtpremnicaId() {
		return otpremnicaId;
	}

	public void setOtpremnicaId(Integer otpremnicaId) {
		this.otpremnicaId = otpremnicaId;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	public Otpremnica getOtpremnica() {
		return otpremnica;
	}

	public void setOtpremnica(Otpremnica otpremnica) {
		this.otpremnica = otpremnica;
	}

}
