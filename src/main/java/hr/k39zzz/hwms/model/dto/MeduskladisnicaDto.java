package hr.k39zzz.hwms.model.dto;

public class MeduskladisnicaDto {
	
	private Integer meduskladisnicaId;
	private Integer proizvodId;
	private String proizvodNaziv;
	private Integer sourceSkladiste;
	private Integer targetSkladiste;
	public Integer getMeduskladisnicaId() {
		return meduskladisnicaId;
	}
	public void setMeduskladisnicaId(Integer meduskladisnicaId) {
		this.meduskladisnicaId = meduskladisnicaId;
	}
	public Integer getProizvodId() {
		return proizvodId;
	}
	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}
	public String getProizvodNaziv() {
		return proizvodNaziv;
	}
	public void setProizvodNaziv(String proizvodNaziv) {
		this.proizvodNaziv = proizvodNaziv;
	}
	public Integer getSourceSkladiste() {
		return sourceSkladiste;
	}
	public void setSourceSkladiste(Integer sourceSkladiste) {
		this.sourceSkladiste = sourceSkladiste;
	}
	public Integer getTargetSkladiste() {
		return targetSkladiste;
	}
	public void setTargetSkladiste(Integer targetSkladiste) {
		this.targetSkladiste = targetSkladiste;
	}

	

}
