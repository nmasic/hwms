package hr.k39zzz.hwms.model.dto;

public class ProizvodPrenesiDto {
	private Integer identifikator;
	private Integer meduskladisnicaId;
	private String meduskladisnicaSifra;
	private Integer proizvodId;
	private String naziv;
	private Integer sourceSkladisteId;
	private Integer targetSkladisteId;
	private Integer prenesiKolicinu;
	private Integer kolicina;
	
	
	
	public Integer getProizvodId() {
		return proizvodId;
	}
	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}
	
	
	public Integer getIdentifikator() {
		return identifikator;
	}
	public void setIdentifikator(Integer identifikator) {
		this.identifikator = identifikator;
	}
	public String getMeduskladisnicaSifra() {
		return meduskladisnicaSifra;
	}
	public void setMeduskladisnicaSifra(String meduskladisnicaSifra) {
		this.meduskladisnicaSifra = meduskladisnicaSifra;
	}
	public Integer getKolicina() {
		return kolicina;
	}
	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Integer getSourceSkladisteId() {
		return sourceSkladisteId;
	}
	public void setSourceSkladisteId(Integer sourceSkladisteId) {
		this.sourceSkladisteId = sourceSkladisteId;
	}
	public Integer getTargetSkladisteId() {
		return targetSkladisteId;
	}
	public void setTargetSkladisteId(Integer targetSkladisteId) {
		this.targetSkladisteId = targetSkladisteId;
	}
	public Integer getPrenesiKolicinu() {
		return prenesiKolicinu;
	}
	public void setPrenesiKolicinu(Integer prenesiKolicinu) {
		this.prenesiKolicinu = prenesiKolicinu;
	}
	public Integer getMeduskladisnicaId() {
		return meduskladisnicaId;
	}
	public void setMeduskladisnicaId(Integer meduskladisnicaId) {
		this.meduskladisnicaId = meduskladisnicaId;
	}
	
	
	

}
