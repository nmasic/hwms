package hr.k39zzz.hwms.model.dto;

import java.util.Date;

public class DokumentDto {

	public enum VrstaDokument {
		PRIMKA, MEDUSKLADISNICA, OTPREMNICA, UKUPNO;
	}

	private VrstaDokument vrsta;

	private String sifra;

	private Date datumKreiranja;

	private int ulaz;

	private int izlaz;

	public DokumentDto() {

	}

	public VrstaDokument getVrsta() {
		return vrsta;
	}

	public void setVrsta(VrstaDokument vrsta) {
		this.vrsta = vrsta;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public int getUlaz() {
		return ulaz;
	}

	public void setUlaz(int ulaz) {
		this.ulaz = ulaz;
	}

	public int getIzlaz() {
		return izlaz;
	}

	public void setIzlaz(int izlaz) {
		this.izlaz = izlaz;
	}

	public void combine(DokumentDto other) {
		ulaz += other.ulaz;
		izlaz += other.izlaz;
	}

}
