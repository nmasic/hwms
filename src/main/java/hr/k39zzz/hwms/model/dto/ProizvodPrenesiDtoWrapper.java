package hr.k39zzz.hwms.model.dto;

import java.util.ArrayList;

public class ProizvodPrenesiDtoWrapper {


private ArrayList<ProizvodPrenesiDto> proizvodiList;

public ArrayList<ProizvodPrenesiDto> getProizvodiList() {
    return proizvodiList;
}

public void setProizvodiList(ArrayList<ProizvodPrenesiDto> proizvodiList) {
    this.proizvodiList = proizvodiList;
}
}