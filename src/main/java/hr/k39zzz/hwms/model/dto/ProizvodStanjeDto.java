package hr.k39zzz.hwms.model.dto;

import java.io.Serializable;

public class ProizvodStanjeDto implements Serializable {

	private Integer proizvodId;

	private Integer skladisteId;

	private String naziv;

	private String opis;

	private String vrsta;

	private Integer kolicina;

	private String skladiste;

	public ProizvodStanjeDto(Integer proizvodId, Integer skladisteId, String naziv, Integer kolicina, String vrsta,
			String skladiste) {
		this.proizvodId = proizvodId;
		this.skladisteId = skladisteId;
		this.naziv = naziv;
		this.kolicina = kolicina;
		this.vrsta = vrsta;
		this.skladiste = skladiste;
	}

	public ProizvodStanjeDto() {
	
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public Integer getSkladisteId() {
		return skladisteId;
	}

	public void setSkladisteId(Integer skladisteId) {
		this.skladisteId = skladisteId;
	}

	public Integer getProizvodId() {
		return proizvodId;
	}

	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}

	public String getSkladiste() {
		return skladiste;
	}

	public void setSkladiste(String skladiste) {
		this.skladiste = skladiste;
	}

}
