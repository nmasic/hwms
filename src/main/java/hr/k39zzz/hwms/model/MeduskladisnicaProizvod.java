package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class MeduskladisnicaProizvod implements Serializable{

	
	
public MeduskladisnicaProizvod(){}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "Kolicina")
	@Min(0)
	@Max(500000)
	private Integer kolicina;
	
	
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ProizvodId")
	private Proizvod proizvod;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MeduskladisnicaId")
	private Meduskladisnica meduskladisnica;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	public Meduskladisnica getMeduskladisnica() {
		return meduskladisnica;
	}

	public void setMeduskladisnica(Meduskladisnica meduskladisnica) {
		this.meduskladisnica = meduskladisnica;
	}
	
	
}


