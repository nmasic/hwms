package hr.k39zzz.hwms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "KorisnikRola")
public class KorisnikRola extends AbstractEntity implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@NotEmpty
	@Column(name = "Rola", length = 15, unique = true)
	private String rola;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "korisnikid")
	private Korisnik korisnik;

	public KorisnikRola() {
	}

	public KorisnikRola(String rola) {
		this.rola = rola;
	}

	@Override
	public String getAuthority() {
		return "ROLE_" + rola;
	}

	public String getRola() {
		return rola;
	}

	public void setRola(String rola) {
		this.rola = rola;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public String toString() {
		return rola;
	}

}
