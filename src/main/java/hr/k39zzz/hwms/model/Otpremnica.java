package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Otpremnica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Size(min = 5, max = 50)
	@Column(name = "Sifra")
	private String sifra;

	@Size(min = 1, max = 100)
	@Column(name = "Izdano")
	private String izdano;

	@NotNull
	@Column(name = "SkladisteId")
	private Integer skladisteId;

	@Column(name = "DatumOtpremanja")
	private Date datumOtpremanja;

	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "otpremnicaProizvodId")
	private List<OtpremnicaProizvod> otpremnicaProizvodList;

	@Transient
	private Skladiste skladiste;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getIzdano() {
		return izdano;
	}

	public void setIzdano(String izdano) {
		this.izdano = izdano;
	}

	public Integer getSkladisteId() {
		return skladisteId;
	}

	public void setSkladisteId(Integer skladisteId) {
		this.skladisteId = skladisteId;
	}

	public Date getDatumOtpremanja() {
		return datumOtpremanja;
	}

	public void setDatumOtpremanja(Date datumOtpremanja) {
		this.datumOtpremanja = datumOtpremanja;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Skladiste getSkladiste() {
		return skladiste;
	}

	public void setSkladiste(Skladiste skladiste) {
		this.skladiste = skladiste;
	}

	public List<OtpremnicaProizvod> getOtpremnicaProizvodList() {
		return otpremnicaProizvodList;
	}

	public void setOtpremnicaProizvodList(List<OtpremnicaProizvod> otpremnicaProizvodList) {
		this.otpremnicaProizvodList = otpremnicaProizvodList;
	}

}
