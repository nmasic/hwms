package hr.k39zzz.hwms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class PrimkaProizvod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "ProizvodId", insertable = false, updatable = false)
	private Integer proizvodId;

	@NotNull
	@Column(name = "PrimkaId", insertable = false, updatable = false)
	private Integer primkaId;

	@Column(name = "Kolicina")
	@Min(1) @Max(1000)
	private Integer kolicina;

	@NotNull
	@Column(name = "DatumKreiranja")
	private Date datumKreiranja;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ProizvodId")
	private Proizvod proizvod;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PrimkaId")
	private Primka primka;

	@Transient
	private List<Proizvod> proizvodi;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProizvodId() {
		return proizvodId;
	}

	public void setProizvodId(Integer proizvodId) {
		this.proizvodId = proizvodId;
	}

	public Integer getPrimkaId() {
		return primkaId;
	}

	public void setPrimkaId(Integer primkaId) {
		this.primkaId = primkaId;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	public Primka getPrimka() {
		return primka;
	}

	public void setPrimka(Primka primka) {
		this.primka = primka;
	}

	public List<Proizvod> getProizvodi() {
		return proizvodi;
	}

	public void setProizvodi(List<Proizvod> proizvodi) {
		this.proizvodi = proizvodi;
	}

}
