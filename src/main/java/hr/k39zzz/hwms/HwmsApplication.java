package hr.k39zzz.hwms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HwmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HwmsApplication.class, args);
	}
}
