package hr.k39zzz.hwms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.k39zzz.hwms.model.Primka;

@Repository
public interface PrimkaRepository extends CrudRepository<Primka, Integer> {
	List<Primka> findAll();

	@Modifying
	@Query("update Primka p set p.sifra = :sifra, p.nazivDobavljaca=:nazivDobavljaca, p.skladisteId = :skladisteId where p.id = :id")
	int updatePrimka(@Param("sifra") String sifra, @Param("nazivDobavljaca") String nazivDobavljaca,
			@Param("skladisteId") int skladisteId, @Param("id") Integer id);

	@Query(value = "select p.sifra sifra, p.datumKreiranja datumKreiranja, pp.kolicina kolicina from Primka p inner join PrimkaProizvod pp on p.id=pp.primkaid where pp.proizvodId=:proizvodId and p.skladisteId=:skladisteId", nativeQuery = true)
	List<Object[]> findAllByProizvodAndSkladiste(@Param("proizvodId") Integer proizvodId, @Param("skladisteId") Integer skladisteId);

	List<Primka> findByNazivDobavljacaLike(String nazivDobavljaca);
	
	Primka findBySifra(String sifra);
}
