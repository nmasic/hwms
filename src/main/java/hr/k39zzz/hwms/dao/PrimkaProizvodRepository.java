package hr.k39zzz.hwms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.k39zzz.hwms.model.PrimkaProizvod;

@Repository
public interface PrimkaProizvodRepository extends CrudRepository<PrimkaProizvod, Integer> {
	List<PrimkaProizvod> findByPrimkaId(Integer primkaId);

	@Query(value = "select sum(ap.kolicina), month(a.datumKreiranja) from Primka a, PrimkaProizvod ap where year(a.datumKreiranja)=:godina group by month(a.datumKreiranja)")
	List<Object[]> sumPrimkaPerMonthByYear(@Param(value = "godina") Integer godina);

	@Query(value = "select sum(ap.kolicina), month(a.datumKreiranja) from Primka a, PrimkaProizvod ap where a.skladisteId=:skladisteId and year(a.datumKreiranja)=:godina group by month(a.datumKreiranja)")
	List<Object[]> sumPrimkaPerMonthByYearAndSkladiste(@Param(value = "godina") Integer godina,
			@Param(value = "skladisteId") Integer skladisteId);

}