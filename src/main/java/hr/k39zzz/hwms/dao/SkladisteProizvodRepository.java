package hr.k39zzz.hwms.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.model.dto.ProizvodStanjeDto;

public interface SkladisteProizvodRepository extends JpaRepository<SkladisteProizvod, Integer> {
	List<SkladisteProizvod> findBySkladisteId(Integer skladisteId);

	@Query("select new hr.k39zzz.hwms.model.dto.ProizvodStanjeDto(p.id, s.id, p.naziv, sp.kolicina, vp.naziv, s.naziv) from Proizvod p "
			+ "inner join p.vrstaProizvoda vp left join p.skladisteProizvod sp left join sp.skladiste s where s.id = ?1")
	List<ProizvodStanjeDto> findProizvodStanjeBySkladisteId(int skladisteId);

	@Modifying
	@Query("update SkladisteProizvod sp set sp.kolicina = :kolicina, sp.datumIzmjene = :datumIzmjene where sp.skladisteId = :skladisteId and sp.proizvodId = :proizvodId")
	int updateSkladisteProizvod(@Param("kolicina") int kolicina, @Param("datumIzmjene") Date datumIzmjene,
			@Param("skladisteId") int skladisteId, @Param("proizvodId") int proizvodId);

	
	SkladisteProizvod findBySkladisteIdAndProizvodId(Integer skladisteId, Integer ProizvodId);
    
	SkladisteProizvod findSkladisteProizvodByProizvodIdAndSkladisteIdAndId(Integer proizvodId,Integer skladisteId,Integer id);

  
}
