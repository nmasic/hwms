package hr.k39zzz.hwms.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import hr.k39zzz.hwms.model.Otpremnica;

public interface OtpremnicaRepository extends CrudRepository<Otpremnica, Integer> {
	List<Otpremnica> findAll();

	List<Otpremnica> findOtpremnicaByIzdanoIgnoreCaseContaining(String izdano);

	@Modifying
	@Query("update Otpremnica o set o.sifra = :sifra, o.izdano=:izdano, o.skladisteId = :skladisteId, o.datumOtpremanja=:datumOtpremanja where o.id = :id")
	int updateOtpremnica(@Param("sifra") String sifra, @Param("izdano") String izdano,
			@Param("skladisteId") int skladisteId, @Param("datumOtpremanja") Date datumOtpremanja,
			@Param("id") Integer id);

	Otpremnica findFirstBySifra(String Sifra);
	
	@Query(value = "select p.sifra sifra, p.datumKreiranja datumKreiranja, pp.kolicina kolicina from Otpremnica p inner join OtpremnicaProizvod pp on p.id=pp.otpremnicaid where pp.proizvodId=:proizvodId and p.skladisteId=:skladisteId", nativeQuery = true)
	List<Object[]> findAllByProizvodAndSkladiste(@Param("proizvodId") Integer proizvodId, @Param("skladisteId") Integer skladisteId);
}
