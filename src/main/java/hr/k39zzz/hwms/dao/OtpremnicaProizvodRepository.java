package hr.k39zzz.hwms.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.k39zzz.hwms.model.OtpremnicaProizvod;

@Repository
public interface OtpremnicaProizvodRepository extends CrudRepository<OtpremnicaProizvod, Integer> {
	List<OtpremnicaProizvod> findByOtpremnicaId(Integer otpremnicaId);

	@Modifying
	@Transactional
	@Query("delete from OtpremnicaProizvod op where op.id = :Id")
	void deleteOtpremnicaProizvodById(@Param("Id") Integer id);

	@Query(value = "select sum(ap.kolicina), month(a.datumKreiranja) from Otpremnica a, OtpremnicaProizvod ap where year(a.datumKreiranja)=:godina group by month(a.datumKreiranja)")
	List<Object[]> sumOtpremnicaPerMonthByYear(@Param(value = "godina") Integer godina);

	@Query(value = "select sum(ap.kolicina), month(a.datumKreiranja) from Otpremnica a, OtpremnicaProizvod ap where a.skladisteId=:skladisteId and year(a.datumKreiranja)=:godina group by month(a.datumKreiranja)")
	List<Object[]> sumOtpremnicaPerMonthByYearAndSkladiste(@Param(value = "godina") Integer godina,
			@Param(value = "skladisteId") Integer skladisteId);

}
