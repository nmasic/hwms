package hr.k39zzz.hwms.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.k39zzz.hwms.model.Meduskladisnica;

import java.util.List;

import javax.transaction.Transactional;


@Repository
public interface MeduskladisnicaRepository  extends JpaRepository<Meduskladisnica, Integer>{
	
	public Meduskladisnica findMeduskladisnicaBySifra(String sifra);
	

}
