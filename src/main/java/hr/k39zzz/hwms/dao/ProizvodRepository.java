package hr.k39zzz.hwms.dao;

import hr.k39zzz.hwms.model.Proizvod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProizvodRepository extends JpaRepository<Proizvod, Integer> {

	List<Proizvod> findAll();
}
