package hr.k39zzz.hwms.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.k39zzz.hwms.model.Korisnik;

@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Integer> {

	Korisnik findByKorisnickoIme(String korisnickoIme);

	@Modifying
	@Transactional
	@Query("update Korisnik k set k.lozinka = :lozinka where k.korisnickoIme = :korisnickoIme")
	int updateLozinka(@Param("lozinka") String lozinka, @Param("korisnickoIme") String korisnickoIme);

	@Modifying
	@Transactional
	@Query("delete from KorisnikRola kr where kr.korisnik.id = :korisnikId")
	int deleteRoleByKorisnikId(@Param("korisnikId") Integer korisnikId);

	@Query("SELECT CASE WHEN COUNT(t) > 0 THEN true ELSE false END FROM Korisnik t WHERE t.korisnickoIme = :korisnickoIme")
	boolean existsByKorisnickoIme(@Param("korisnickoIme") String korisnickoIme);

}
