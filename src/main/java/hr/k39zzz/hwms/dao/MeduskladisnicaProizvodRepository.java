package hr.k39zzz.hwms.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.k39zzz.hwms.model.MeduskladisnicaProizvod;

public interface MeduskladisnicaProizvodRepository extends JpaRepository<MeduskladisnicaProizvod,Integer> {

}
