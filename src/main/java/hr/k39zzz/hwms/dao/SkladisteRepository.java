package hr.k39zzz.hwms.dao;

import hr.k39zzz.hwms.model.Skladiste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Nikola on 28.5.2016..
 */
@Repository
public interface SkladisteRepository extends JpaRepository<Skladiste, Integer> {
    List<Skladiste> findAll();
}
