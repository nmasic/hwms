package hr.k39zzz.hwms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.k39zzz.hwms.model.VrstaProizvoda;


public interface VrstaProizvodaRepository extends JpaRepository<VrstaProizvoda, Integer> {

	List<VrstaProizvoda> findAll();
}
