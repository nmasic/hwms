package hr.k39zzz.hwms.view;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.supercsv.io.ICsvBeanWriter;

import hr.k39zzz.hwms.model.Proizvod;

/**
 * An implementation of the AbstractCsvView. This decides what model data to be
 * used to generate CSV data.
 * 
 * @author www.codejava.net
 *
 */
public class CsvViewImpl extends AbstractCsvView {

	@Override
	protected void buildCsvDocument(ICsvBeanWriter csvWriter, Map<String, Object> model) throws IOException {

		@SuppressWarnings("unchecked")
		List<Proizvod> listProizvod = (List<Proizvod>) model.get("csvData");
		String[] header = (String[]) model.get("csvHeader");

		csvWriter.writeHeader(header);

		for (Proizvod pr : listProizvod) {
			csvWriter.write(pr, header);
		}
	}
}