package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.VrstaProizvoda;

public interface VrstaProizvodaService {

	 List<VrstaProizvoda> fetchAllVrstaProizvoda();

	VrstaProizvoda fetchVrstaProizvodaById(Integer id);
}
