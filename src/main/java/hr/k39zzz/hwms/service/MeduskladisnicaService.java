package hr.k39zzz.hwms.service;





import java.util.List;

import hr.k39zzz.hwms.model.Meduskladisnica;
import hr.k39zzz.hwms.model.Otpremnica;
import hr.k39zzz.hwms.model.Skladiste;

public interface MeduskladisnicaService {
	List<Meduskladisnica> fetchAllMeduskladisnica();
	Meduskladisnica findMeduskladisnicaById(int meduskladisnicaId);
	 Meduskladisnica saveMeduskladisnica(Meduskladisnica meduskladisnica);
	  Meduskladisnica findMeduskladisnicaBySifra(String sifra);
		
	
	
}
