package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.Korisnik;

public interface KorisnikService {

	int updateLozinka(String lozinka, String korisnickoIme);

	Korisnik findByKorisnickoIme(String korisnickoIme);

	boolean lozinkeNeOdgovaraju(String lozinkaForm, String lozinkaUsera);

	List<Korisnik> findAll();

	Korisnik saveKorisnik(Korisnik korisnik);

	Korisnik getById(Integer id);

	boolean postojiByKorisnickoIme(String korisnickoIme);
}
