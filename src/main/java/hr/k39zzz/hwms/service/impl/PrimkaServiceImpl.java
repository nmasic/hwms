package hr.k39zzz.hwms.service.impl;

import hr.k39zzz.hwms.dao.PrimkaProizvodRepository;
import hr.k39zzz.hwms.dao.PrimkaRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteRepository;
import hr.k39zzz.hwms.model.Primka;
import hr.k39zzz.hwms.model.PrimkaProizvod;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.service.PrimkaService;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PrimkaServiceImpl implements PrimkaService {

	@Autowired
	private PrimkaRepository primkaRepository;

	@Autowired
	private PrimkaProizvodRepository primkaProizvodRepository;

	@Autowired
	private ProizvodRepository proizvodRepository;

	@Autowired
	private SkladisteRepository skladisteRepository;

	@Override
	public List<Primka> findAllPrimka() {
		List<Primka> primkaList = primkaRepository.findAll();

		for (Primka primka : primkaList) {
			primka.setSkladiste(skladisteRepository.getOne(primka.getSkladisteId()));
		}
		return primkaList;
	}

	@Override
	public Primka findPrimkaById(int primkaId) {
		return primkaRepository.findOne(primkaId);
	}

	@Override
	public Primka findBySifra(String sifra) {
		return primkaRepository.findBySifra(sifra);
	}

	@Override
	public List<PrimkaProizvod> findAllPrimkaProizvod(Integer primkaId) {
		List<PrimkaProizvod> primkaProizvodList = primkaProizvodRepository.findByPrimkaId(primkaId);
		for (PrimkaProizvod pp : primkaProizvodList) {
			pp.setProizvod(proizvodRepository.findOne(pp.getProizvodId()));
			pp.setPrimka(primkaRepository.findOne(pp.getPrimkaId()));
			pp.setProizvodi(proizvodRepository.findAll());
		}
		return primkaProizvodList;
	}

	@Override
	public List<Proizvod> findAllProizvodi() {
		return proizvodRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Primka savePrimka(Primka primka) {
		primka.setDatumKreiranja(new Date());

		return primkaRepository.save(primka);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int updatePrimka(String sifra, String nazivDobavljaca, Integer skladisteId, Integer id) {
		return primkaRepository.updatePrimka(sifra, nazivDobavljaca, skladisteId, id);
	}

	@Override
	public List<Object[]> findAllPrimkaByProizvodAndSkladiste(Integer proizvodId, Integer skladisteId) {
		return primkaRepository.findAllByProizvodAndSkladiste(proizvodId, skladisteId);
	}
	
	@Override
	public List<Primka> findPrimkaByDobavljac(String nazivDobavljaca) {
		
		List<Primka> primkaList = primkaRepository.findByNazivDobavljacaLike("%" + nazivDobavljaca + "%");
		for (Primka primka : primkaList) {
			primka.setSkladiste(skladisteRepository.getOne(primka.getSkladisteId()));
		}
		return primkaList;
	}
}
