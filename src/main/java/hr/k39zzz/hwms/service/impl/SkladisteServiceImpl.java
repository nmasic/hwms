package hr.k39zzz.hwms.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteRepository;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.service.SkladisteService;

/**
 * Created by Nikola on 28.5.2016..
 */
@Service
public class SkladisteServiceImpl implements SkladisteService {

	@Autowired
	private SkladisteRepository skladisteRepository;

	@Autowired
	private SkladisteProizvodRepository skladisteProizvodRepository;

	@Autowired
	private ProizvodRepository proizvodRepository;

	@Override
	@Cacheable(value = "skladista")
	public List<Skladiste> fetchAllSkladiste() {
		return skladisteRepository.findAll();
	}

	@Override
	public List<Proizvod> findAllProizvodiForSkladisteId(int skladisteId) {
		List<SkladisteProizvod> skladisteProizvodList = skladisteProizvodRepository.findBySkladisteId(skladisteId);
		List<Proizvod> proizvodList = new ArrayList<>();
		for (SkladisteProizvod sp : skladisteProizvodList) {
			proizvodList.add(proizvodRepository.findOne(sp.getProizvodId()));
		}
		return proizvodList;
	}

	@Override
	public Skladiste fetchSkladisteById(int skladisteId) {
		return skladisteRepository.findOne(skladisteId);
	}

	@Override
	@CacheEvict(value = "skladista", allEntries = true)
	public Skladiste saveSkladiste(Skladiste skladiste) {
		skladiste.setDatumKreiranja(new Date());
		return skladisteRepository.save(skladiste);
	}

}
