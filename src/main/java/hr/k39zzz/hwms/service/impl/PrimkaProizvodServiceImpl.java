package hr.k39zzz.hwms.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.dao.PrimkaProizvodRepository;
import hr.k39zzz.hwms.dao.PrimkaRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteRepository;
import hr.k39zzz.hwms.model.PrimkaProizvod;
import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.service.PrimkaProizvodService;

@Service
public class PrimkaProizvodServiceImpl implements PrimkaProizvodService {

	@Autowired
	PrimkaProizvodRepository primkaProizvodRepository;

	@Autowired
	PrimkaRepository primkaRepository;

	@Autowired
	SkladisteProizvodRepository skladisteProizvodRepository;

	@Autowired
	SkladisteRepository skladisteRepository;

	@Autowired
	ProizvodRepository proizvodRepository;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void savePrimkaProizvodList(List<PrimkaProizvod> primkaProizvodList, Integer primkaId) {
		for (PrimkaProizvod pp : primkaProizvodList) {
			pp.setDatumKreiranja(new Date());
			pp.setProizvodId(pp.getProizvod().getId());
			pp.setPrimkaId(primkaId);
			pp.setPrimka(primkaRepository.findOne(primkaId));

			SkladisteProizvod sp = skladisteProizvodRepository
					.findBySkladisteIdAndProizvodId(pp.getPrimka().getSkladisteId(), pp.getProizvodId());
			Integer kol = new Integer(0);

			if (sp == null) {
				SkladisteProizvod skladisteProizvod = new SkladisteProizvod();
				skladisteProizvod.setSkladisteId(pp.getPrimka().getSkladisteId());
				skladisteProizvod.setProizvodId(pp.getProizvodId());
				skladisteProizvod.setKolicina(pp.getKolicina());
				skladisteProizvod.setDatumKreiranja(new Date());
				skladisteProizvod.setSkladiste(skladisteRepository.findOne(pp.getPrimka().getSkladisteId()));
				skladisteProizvod.setProizvod(proizvodRepository.findOne(pp.getProizvodId()));
				skladisteProizvodRepository.save(skladisteProizvod);
			} else {
				kol = sp.getKolicina();
				kol += pp.getKolicina();
				skladisteProizvodRepository.updateSkladisteProizvod(kol, new Date(), pp.getPrimka().getSkladisteId(),
						pp.getProizvodId());
			}

			primkaProizvodRepository.save(pp);
		}
	}

	@Override
	public List<Object[]> sumPrimkaPerMonthByYearAndSkladiste(Integer godina, Integer skladisteId) {

		if (Objects.isNull(skladisteId)) {
			return primkaProizvodRepository.sumPrimkaPerMonthByYear(godina);
		}
		return primkaProizvodRepository.sumPrimkaPerMonthByYearAndSkladiste(godina, skladisteId);
	}

}
