package hr.k39zzz.hwms.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.dao.OtpremnicaProizvodRepository;
import hr.k39zzz.hwms.dao.OtpremnicaRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteRepository;
import hr.k39zzz.hwms.model.OtpremnicaProizvod;
import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.service.OtpremnicaProizvodService;

@Service
public class OtpremnicaProizvodServiceImpl implements OtpremnicaProizvodService {

	@Autowired
	OtpremnicaProizvodRepository otpremnicaProizvodRepository;

	@Autowired
	OtpremnicaRepository otpremnicaRepository;

	@Autowired
	ProizvodRepository proizvodRepository;

	@Autowired
	SkladisteProizvodRepository skladisteProizvodRepository;

	@Autowired
	SkladisteRepository skladisteRepository;

	@Override
	public List<OtpremnicaProizvod> findOtpremnicaProizvodByOtpremnicaId(Integer otpremnicaId) {
		List<OtpremnicaProizvod> otpremnicaProizvodList = otpremnicaProizvodRepository.findByOtpremnicaId(otpremnicaId);
		// dodana lamda
		otpremnicaProizvodList.forEach(op -> {
			op.setProizvod(proizvodRepository.findOne(op.getProizvodId()));
			op.setOtpremnica(otpremnicaRepository.findOne(op.getOtpremnicaId()));
		});
		// for (OtpremnicaProizvod op : otpremnicaProizvodList) {
		// op.setProizvod(proizvodRepository.findOne(op.getProizvodId()));
		// op.setOtpremnica(otpremnicaRepository.findOne(op.getOtpremnicaId()));
		// // op.setProizvodi(proizvodRepository.findAll());
		// }
		return otpremnicaProizvodList;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOtpremnicaProizvodList(List<OtpremnicaProizvod> otpremnicaProizvodList, Integer otpremnicaId) {
		for (OtpremnicaProizvod op : otpremnicaProizvodList) {
			op.setDatumKreiranja(new Date());
			op.setProizvodId(op.getProizvod().getId());
			op.setOtpremnicaId(otpremnicaId);
			op.setOtpremnica(otpremnicaRepository.findOne(otpremnicaId));

			SkladisteProizvod sp = skladisteProizvodRepository
					.findBySkladisteIdAndProizvodId(op.getOtpremnica().getSkladisteId(), op.getProizvodId());
			Integer kol = new Integer(0);

			if (sp == null) {
				SkladisteProizvod skladisteProizvod = new SkladisteProizvod();
				skladisteProizvod.setSkladisteId(op.getOtpremnica().getSkladisteId());
				skladisteProizvod.setProizvodId(op.getProizvodId());
				skladisteProizvod.setKolicina(op.getKolicina());
				skladisteProizvod.setDatumKreiranja(new Date());
				skladisteProizvod.setSkladiste(skladisteRepository.findOne(op.getOtpremnica().getSkladisteId()));
				skladisteProizvod.setProizvod(proizvodRepository.findOne(op.getProizvodId()));
				skladisteProizvodRepository.save(skladisteProizvod);
			} else {
				kol = sp.getKolicina();
				kol -= op.getKolicina();
				skladisteProizvodRepository.updateSkladisteProizvod(kol, new Date(),
						op.getOtpremnica().getSkladisteId(), op.getProizvodId());
			}

			otpremnicaProizvodRepository.save(op);
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteOtpremnicaProizvodList(List<OtpremnicaProizvod> otpremnicaProizvodList, Integer otpremnicaId) {
		for (OtpremnicaProizvod op : otpremnicaProizvodList) {
			SkladisteProizvod sp = skladisteProizvodRepository
					.findBySkladisteIdAndProizvodId(op.getOtpremnica().getSkladisteId(), op.getProizvodId());
			Integer kol = new Integer(0);
			kol = sp.getKolicina();
			kol += op.getKolicina();
			skladisteProizvodRepository.updateSkladisteProizvod(kol, new Date(), op.getOtpremnica().getSkladisteId(),
					op.getProizvodId());
			otpremnicaProizvodRepository.deleteOtpremnicaProizvodById(op.getId());

		}
	}

	@Override
	public List<Object[]> sumOtpremnicaPerMonthByYearAndSkladiste(Integer godina, Integer skladisteId) {
		if (Objects.isNull(skladisteId)) {
			return otpremnicaProizvodRepository.sumOtpremnicaPerMonthByYear(godina);
		}
		return otpremnicaProizvodRepository.sumOtpremnicaPerMonthByYearAndSkladiste(godina, skladisteId);
	}
}
