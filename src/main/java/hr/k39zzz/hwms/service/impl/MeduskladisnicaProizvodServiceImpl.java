package hr.k39zzz.hwms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.MeduskladisnicaProizvodRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.model.MeduskladisnicaProizvod;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.service.MeduskladisnicaProizvodService;
@Service
public class MeduskladisnicaProizvodServiceImpl implements MeduskladisnicaProizvodService {

	@Autowired
	MeduskladisnicaProizvodRepository meduskladisnicaProizvodRepository;
	@Autowired
	ProizvodRepository proizvodRepository;
	
	
	public List<Object> fetchProizvodiByMeduskladisnicaId(int meduskladisnicaId){
		List<String> listProizvodi=new ArrayList<>();
		List<Integer> listKolicine=new ArrayList<>();
		List<Object> list=new ArrayList<>();
		List<MeduskladisnicaProizvod> lista=meduskladisnicaProizvodRepository.findAll();
	   HashMap<Integer,Proizvod> mapa=new HashMap<>();
	   
	   for(MeduskladisnicaProizvod mp:lista){
		   
            if (mp.getMeduskladisnica().getId()==meduskladisnicaId){	
	       Proizvod proizvod=proizvodRepository.findOne(mp.getProizvod().getId());
	       listProizvodi.add(proizvod.getNaziv());
	       listKolicine.add(mp.getKolicina());
	   
	   }
	   }
	   list.add(listProizvodi);
	   list.add(listKolicine);
            return list;
            
      }
	 public MeduskladisnicaProizvod saveMeduskladisnicaProizvod(MeduskladisnicaProizvod meduskladisnicaProizvod){
		return meduskladisnicaProizvodRepository.save(meduskladisnicaProizvod);
		
	}
	   
	  
	
	
	
	}
	
	
	
	
	
	

