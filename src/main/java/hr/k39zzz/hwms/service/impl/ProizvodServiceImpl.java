package hr.k39zzz.hwms.service.impl;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.OtpremnicaRepository;
import hr.k39zzz.hwms.dao.PrimkaRepository;
import hr.k39zzz.hwms.dao.ProizvodRepository;
import hr.k39zzz.hwms.dao.SkladisteProizvodRepository;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.dto.DokumentDto;
import hr.k39zzz.hwms.model.dto.DokumentDto.VrstaDokument;
import hr.k39zzz.hwms.model.dto.ProizvodStanjeDto;
import hr.k39zzz.hwms.service.ProizvodService;

@Service
public class ProizvodServiceImpl implements ProizvodService {

	@Autowired
	private ProizvodRepository proizvodRepository;

	@Autowired
	private SkladisteProizvodRepository skladisteProizvodRepository;

	@Autowired
	private PrimkaRepository primkaRepository;

	@Autowired
	private OtpremnicaRepository otpremnnicaRepository;

	@Override
	public List<Proizvod> fetchAllProizvod() {
		return proizvodRepository.findAll();
	}

	@Override
	public Proizvod saveProizvod(Proizvod proizvod) {
		return proizvodRepository.save(proizvod);
	}

	@Override
	public Proizvod findProizvodById(int proizvodId) {
		return proizvodRepository.findOne(proizvodId);
	}

	@Override
	public List<ProizvodStanjeDto> findProizvodStanjeBySkladisteId(int skladisteId) {
		return skladisteProizvodRepository.findProizvodStanjeBySkladisteId(skladisteId);
	}

	@Override
	public List<DokumentDto> sviDokumentZaProizvodSkladiste(int skladisteId, int proizvodId) {

		List<Object[]> svePrimke = primkaRepository.findAllByProizvodAndSkladiste(proizvodId, skladisteId);
		List<Object[]> sveOtpremnice = otpremnnicaRepository.findAllByProizvodAndSkladiste(proizvodId, skladisteId);

		Comparator<DokumentDto> byDatumKreiranja = (e1, e2) -> e1.getDatumKreiranja().compareTo(e2.getDatumKreiranja());

		List<DokumentDto> sviDokumenti = Stream.concat(
							svePrimke.stream().map(convertToPrimkaDto), // 
							sveOtpremnice.stream().map(convertToOtpremnicaDto)) //
				.sorted(byDatumKreiranja) //
				.collect(Collectors.<DokumentDto> toList());

		DokumentDto ukupno = sviDokumenti.stream().collect(DokumentDto::new, DokumentDto::combine, DokumentDto::combine);
		ukupno.setVrsta(VrstaDokument.UKUPNO);
		sviDokumenti.add(ukupno);

		return sviDokumenti;
	}

	Function<Object[], DokumentDto> convertToPrimkaDto = new Function<Object[], DokumentDto>() {

		public DokumentDto apply(Object[] izvor) {
			DokumentDto dokumentDto = new DokumentDto();
			dokumentDto.setVrsta(VrstaDokument.PRIMKA);
			dokumentDto.setSifra((String) izvor[0]);
			dokumentDto.setDatumKreiranja((Date) izvor[1]);
			dokumentDto.setUlaz((int) izvor[2]);
			return dokumentDto;
		}
	};
	
	Function<Object[], DokumentDto> convertToOtpremnicaDto = new Function<Object[], DokumentDto>() {

		public DokumentDto apply(Object[] izvor) {
			DokumentDto dokumentDto = new DokumentDto();
			dokumentDto.setVrsta(VrstaDokument.OTPREMNICA);
			dokumentDto.setSifra((String) izvor[0]);
			dokumentDto.setDatumKreiranja((Date) izvor[1]);
			dokumentDto.setIzlaz((int) izvor[2]);
			return dokumentDto;
		}
	};

}
