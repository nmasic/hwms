package hr.k39zzz.hwms.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.dao.OtpremnicaProizvodRepository;
import hr.k39zzz.hwms.dao.OtpremnicaRepository;
import hr.k39zzz.hwms.dao.SkladisteRepository;
import hr.k39zzz.hwms.model.Otpremnica;
import hr.k39zzz.hwms.service.OtpremnicaService;

@Service
public class OtpremnicaServiceImpl implements OtpremnicaService {

	@Autowired
	private OtpremnicaRepository otpremnicaRepository;

	@Autowired
	private OtpremnicaProizvodRepository otpremnicaProizvodRepository;

	@Autowired
	private SkladisteRepository skladisteRepository;

	public List<Otpremnica> findAllOtpremnica() {

		List<Otpremnica> otpremnicaList = otpremnicaRepository.findAll();
		// dodana lamda
		otpremnicaList.forEach(otpremnica -> {
			otpremnica.setSkladiste(skladisteRepository.getOne(otpremnica.getSkladisteId()));
			otpremnica.setOtpremnicaProizvodList(otpremnicaProizvodRepository.findByOtpremnicaId(otpremnica.getId()));
		});

		// for (Otpremnica otpremnica : otpremnicaList) {
		// otpremnica.setSkladiste(skladisteRepository.getOne(otpremnica.getSkladisteId()));
		// otpremnica.setOtpremnicaProizvodList(otpremnicaProizvodRepository.findByOtpremnicaId(otpremnica.getId()));
		// }
		return otpremnicaList;
	}

	@Override
	public List<Otpremnica> findOtpremnicaByIzdano(String izdano) {

		List<Otpremnica> otpremnicaList = otpremnicaRepository.findOtpremnicaByIzdanoIgnoreCaseContaining(izdano);

		for (Otpremnica otpremnica : otpremnicaList) {
			otpremnica.setSkladiste(skladisteRepository.getOne(otpremnica.getSkladisteId()));
			otpremnica.setOtpremnicaProizvodList(otpremnicaProizvodRepository.findByOtpremnicaId(otpremnica.getId()));
		}
		return otpremnicaList;
	}

	@Override
	public Otpremnica findOtpremnicaById(int otpremnicaId) {
		return otpremnicaRepository.findOne(otpremnicaId);
	}

	@Override
	public Otpremnica findBySifra(String Sifra) {
		return otpremnicaRepository.findFirstBySifra(Sifra);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Otpremnica saveOtpremnica(Otpremnica otpremnica) {
		otpremnica.setDatumKreiranja(new Date());
		otpremnica.setDatumOtpremanja(new Date());

		return otpremnicaRepository.save(otpremnica);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateOtpremnica(Otpremnica otpremnica) {

		otpremnica.setSkladiste(skladisteRepository.findOne(otpremnica.getSkladisteId()));
		otpremnicaRepository.updateOtpremnica(otpremnica.getSifra(), otpremnica.getIzdano(),
				otpremnica.getSkladisteId(), new Date(), otpremnica.getId());
	}
}
