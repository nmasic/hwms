package hr.k39zzz.hwms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.SkladisteProizvodRepository;
import hr.k39zzz.hwms.model.Meduskladisnica;
import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.SkladisteProizvod;
import hr.k39zzz.hwms.service.SkladisteProizvodService;

@Service
public class SkladisteProizvodServiceImpl implements SkladisteProizvodService {
	
	@Autowired
	SkladisteProizvodRepository skladisteProizvodRepository;
	
	
	
	
	    @Override
		public List<SkladisteProizvod> fetchAllSkladisteProizvod() {
			return skladisteProizvodRepository.findAll();
		}
	    @Override
	    public SkladisteProizvod findSkladisteProizvodByProizvodIdAndSkladisteIdAndId(Integer proizvodId,Integer skladisteId,Integer id){
	    	
	    	
	     return  skladisteProizvodRepository.findSkladisteProizvodByProizvodIdAndSkladisteIdAndId(proizvodId, skladisteId,id);
	    }
	    @Override
		public SkladisteProizvod saveSkladisteProizvod(SkladisteProizvod skladisteProizvod) {
			
			return skladisteProizvodRepository.save(skladisteProizvod);
		}

}
