package hr.k39zzz.hwms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.dao.KorisnikRepository;
import hr.k39zzz.hwms.model.Korisnik;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(final String korisnickoIme) throws UsernameNotFoundException {
		final Korisnik korisnik = korisnikRepository.findByKorisnickoIme(korisnickoIme);
		logger.trace("Korisnik : {}", korisnik);
		if (korisnik == null) {
			logger.trace("Korisnik nije naden.");
			throw new UsernameNotFoundException("Korisnik nije naden.");
		}
		return korisnik;
	}

}
