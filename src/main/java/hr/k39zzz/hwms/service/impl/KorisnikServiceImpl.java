package hr.k39zzz.hwms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.dao.KorisnikRepository;
import hr.k39zzz.hwms.model.Korisnik;
import hr.k39zzz.hwms.model.KorisnikRola;
import hr.k39zzz.hwms.service.KorisnikService;

@Service
public class KorisnikServiceImpl implements KorisnikService {

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int updateLozinka(String lozinka, String korisnickoIme) {
		String encryptLozinka = passwordEncoder.encode(lozinka);
		return korisnikRepository.updateLozinka(encryptLozinka, korisnickoIme);
	}

	@Override
	public Korisnik findByKorisnickoIme(String korisnickoIme) {
		return korisnikRepository.findByKorisnickoIme(korisnickoIme);
	}

	@Override
	public boolean lozinkeNeOdgovaraju(String lozinkaForm, String lozinkaUsera) {
		return !passwordEncoder.matches(lozinkaForm, lozinkaUsera);
	}

	@Override
	public List<Korisnik> findAll() {
		return (List<Korisnik>) korisnikRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Korisnik saveKorisnik(Korisnik korisnik) {

		if (korisnik.getId() != null) {
			korisnikRepository.deleteRoleByKorisnikId(korisnik.getId());
		}

		for (KorisnikRola rola : korisnik.getRole()) {
			rola.setKorisnik(korisnik);
			rola.setKorisnikKreiranja(korisnik.getKorisnikKreiranja() == null ? korisnik.getKorisnikIzmjene()
					: korisnik.getKorisnikKreiranja());
		}

		return korisnikRepository.save(korisnik);
	}

	@Override
	public Korisnik getById(Integer id) {
		return korisnikRepository.findOne(id);
	}

	@Override
	public boolean postojiByKorisnickoIme(String korisnickoIme) {
		return korisnikRepository.existsByKorisnickoIme(korisnickoIme);
	}
}
