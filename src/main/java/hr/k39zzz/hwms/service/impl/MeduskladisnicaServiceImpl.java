package hr.k39zzz.hwms.service.impl;





import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.MeduskladisnicaRepository;
import hr.k39zzz.hwms.model.Meduskladisnica;
import hr.k39zzz.hwms.model.Otpremnica;
import hr.k39zzz.hwms.model.Skladiste;
import hr.k39zzz.hwms.service.MeduskladisnicaService;



@Service
public class MeduskladisnicaServiceImpl implements MeduskladisnicaService{
	
	  
	
	    
	    @Autowired
	    private MeduskladisnicaRepository meduskladisnicaRepository;
	    
	    
	    
	    @Override
		public List<Meduskladisnica> fetchAllMeduskladisnica() {
			return meduskladisnicaRepository.findAll();
		}
	    public Meduskladisnica findMeduskladisnicaById(int meduskladisnicaId) {
			return meduskladisnicaRepository.findOne(meduskladisnicaId);
		}
	    @Override
		public Meduskladisnica saveMeduskladisnica(Meduskladisnica meduskladisnica) {
			
			return meduskladisnicaRepository.save(meduskladisnica);
		}
	     
	    public Meduskladisnica findMeduskladisnicaBySifra(String sifra){
	    	
	    return	meduskladisnicaRepository.findMeduskladisnicaBySifra(sifra);
	    	
	    }
		
	       	
	    
     	

}
