package hr.k39zzz.hwms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.k39zzz.hwms.dao.VrstaProizvodaRepository;
import hr.k39zzz.hwms.model.VrstaProizvoda;
import hr.k39zzz.hwms.service.VrstaProizvodaService;

@Service
public class VrstaProizvodaServiceImpl  implements VrstaProizvodaService {

	@Autowired
    private VrstaProizvodaRepository vrstaProizvodaRepository;
	
	@Override
	public List<VrstaProizvoda> fetchAllVrstaProizvoda() {
		return vrstaProizvodaRepository.findAll();
	}
	@Override
	public VrstaProizvoda fetchVrstaProizvodaById(Integer id) {
		
		
	   return vrstaProizvodaRepository.findOne(id);
	}
	
}