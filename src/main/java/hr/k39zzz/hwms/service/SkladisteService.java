package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.Skladiste;

/**
 * Created by Nikola on 28.5.2016..
 */
public interface SkladisteService {
	List<Skladiste> fetchAllSkladiste();

	List<Proizvod> findAllProizvodiForSkladisteId(int skladisteId);

	Skladiste fetchSkladisteById(int skladisteId);

	Skladiste saveSkladiste(Skladiste skladiste);

}
