package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.SkladisteProizvod;

public interface SkladisteProizvodService {
	public List<SkladisteProizvod> fetchAllSkladisteProizvod();
	public SkladisteProizvod findSkladisteProizvodByProizvodIdAndSkladisteIdAndId(Integer proizvodId,Integer skladisteId,Integer id);
	public SkladisteProizvod saveSkladisteProizvod(SkladisteProizvod skladisteProizvod);
}
