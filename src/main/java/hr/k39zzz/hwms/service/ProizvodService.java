package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.Proizvod;
import hr.k39zzz.hwms.model.dto.DokumentDto;
import hr.k39zzz.hwms.model.dto.ProizvodStanjeDto;

public interface ProizvodService {
	
    List<Proizvod> fetchAllProizvod();
    
	Proizvod saveProizvod(Proizvod proizvod);
	
	Proizvod findProizvodById(int proizvodId);
	
	List<ProizvodStanjeDto> findProizvodStanjeBySkladisteId(int skladisteId);

	List<DokumentDto> sviDokumentZaProizvodSkladiste(int skladisteId, int proizvodId);
}

