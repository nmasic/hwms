package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.OtpremnicaProizvod;

public interface OtpremnicaProizvodService {
	List<OtpremnicaProizvod> findOtpremnicaProizvodByOtpremnicaId(Integer otpremnicaId);

	void saveOtpremnicaProizvodList(List<OtpremnicaProizvod> otpremnicaProizvodList, Integer otpremnicaId);

	void deleteOtpremnicaProizvodList(List<OtpremnicaProizvod> otpremnicaProizvodList, Integer otpremnicaId);

	List<Object[]> sumOtpremnicaPerMonthByYearAndSkladiste(Integer godina, Integer skladisteId);
}
