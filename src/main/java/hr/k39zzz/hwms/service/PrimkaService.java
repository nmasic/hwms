package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.Primka;
import hr.k39zzz.hwms.model.PrimkaProizvod;
import hr.k39zzz.hwms.model.Proizvod;

public interface PrimkaService {
	List<Primka> findAllPrimka();

	Primka findPrimkaById(int primkaId);

	List<PrimkaProizvod> findAllPrimkaProizvod(Integer primkaId);

	List<Proizvod> findAllProizvodi();

	Primka savePrimka(Primka primka);

	int updatePrimka(String sifra, String nazivDobavljaca, Integer skladisteId, Integer id);

	List<Primka> findPrimkaByDobavljac(String nazivDobavljaca);

	List<Object[]> findAllPrimkaByProizvodAndSkladiste(Integer proizvodId, Integer skladisteId);

	Primka findBySifra(String string);
}
