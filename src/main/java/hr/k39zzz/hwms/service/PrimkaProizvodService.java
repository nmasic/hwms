package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.PrimkaProizvod;

public interface PrimkaProizvodService {
	void savePrimkaProizvodList(List<PrimkaProizvod> primkaProizvodList, Integer primkaId);

	List<Object[]> sumPrimkaPerMonthByYearAndSkladiste(Integer godina, Integer skladisteId);
}
