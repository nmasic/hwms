package hr.k39zzz.hwms.service;

import java.util.List;

import hr.k39zzz.hwms.model.Otpremnica;

public interface OtpremnicaService {
	List<Otpremnica> findAllOtpremnica();

	List<Otpremnica> findOtpremnicaByIzdano(String izdano);

	Otpremnica findOtpremnicaById(int otpremnicaId);

	Otpremnica saveOtpremnica(Otpremnica otpremnica);

	void updateOtpremnica(Otpremnica otpremnica);

	Otpremnica findBySifra(String Sifra);

}
