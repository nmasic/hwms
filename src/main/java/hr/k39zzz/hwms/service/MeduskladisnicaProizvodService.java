package hr.k39zzz.hwms.service;

import java.util.HashMap;
import java.util.List;

import hr.k39zzz.hwms.model.MeduskladisnicaProizvod;
import hr.k39zzz.hwms.model.Proizvod;

public interface MeduskladisnicaProizvodService {

	List<Object> fetchProizvodiByMeduskladisnicaId(int meduskladisnicaId);
    MeduskladisnicaProizvod saveMeduskladisnicaProizvod(MeduskladisnicaProizvod meduskladisnicaProizvod);
}
