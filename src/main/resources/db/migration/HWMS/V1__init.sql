CREATE TABLE HWMS.Korisnik (
Id int not null auto_increment primary key,
KorisnickoIme varchar(100) not null,
Lozinka varchar(60) not null,
Ime varchar(30) not null,
Prezime  varchar(50) not null,
Email varchar(200) not null,
Aktivan char(1) not null default '1',
DatumKreiranja datetime not null,
KorisnikKreiranja varchar(100) not null,
DatumIzmjene datetime,
KorisnikIzmjene varchar(100),
UNIQUE (KorisnickoIme)
);

CREATE TABLE HWMS.KorisnikRola (
Id int not null auto_increment primary key,
KorisnikId int not null,
Rola varchar(64) not null,
DatumKreiranja datetime not null,
KorisnikKreiranja varchar(100) not null,
DatumIzmjene datetime,
KorisnikIzmjene varchar(100),
UNIQUE (KorisnikId, Rola),
FOREIGN KEY (KorisnikId) REFERENCES HWMS.Korisnik(Id)
);

INSERT INTO HWMS.Korisnik(Id, KorisnickoIme, Lozinka, Ime, Prezime, Email, DatumKreiranja, KorisnikKreiranja)
VALUES (1,'admin','$2a$10$UQp5ICn.jjBwkJKkYIG4TOHHI8wPGamJ5n5jZe8d2Xe0q18BLZyXG', 'Ivan', 'Horvat','admin@hwms.hr', current_timestamp, 'batch');

INSERT INTO HWMS.Korisnik(Id, KorisnickoIme, Lozinka, Ime, Prezime, Email, DatumKreiranja, KorisnikKreiranja)
VALUES (2,'korisnik','$2a$10$lO.kkEkGS2bD8Enco076R./DvU5543AVerf6pkUe7l7b4YJg26T6S', 'Marko', 'Horvat','korisnik@hwms.hr', current_timestamp, 'batch');

INSERT INTO HWMS.KorisnikRola(Id, KorisnikId, Rola, DatumKreiranja, KorisnikKreiranja)
VALUES (1, 1, 'ADMIN', current_timestamp, 'batch');

INSERT INTO HWMS.KorisnikRola(Id, KorisnikId, Rola, DatumKreiranja, KorisnikKreiranja)
VALUES (2, 2, 'KORISNIK', current_timestamp, 'batch');

