CREATE TABLE HWMS.Otpremnica (
Id int not null auto_increment primary key, 	
Sifra varchar(50),
Izdano varchar(100),
SkladisteId int not null,
DatumOtpremanja datetime,
DatumKreiranja datetime,
FOREIGN KEY(SkladisteId) REFERENCES HWMS.Skladiste(Id)
);

CREATE TABLE HWMS.OtpremnicaProizvod (
Id int not null auto_increment primary key,
ProizvodId int not null,
OtpremnicaId int not null,
Kolicina int,
DatumKreiranja datetime,
FOREIGN KEY(ProizvodId) REFERENCES HWMS.Proizvod(Id),
FOREIGN KEY(OtpremnicaId) REFERENCES HWMS.Otpremnica(Id)
);

INSERT INTO HWMS.Otpremnica(Id, Sifra, Izdano, SkladisteId, DatumKreiranja,DatumOtpremanja)
VALUES (1, '00001', 'Marko i kćeri', 1, current_timestamp,'2016-05-02');

INSERT INTO HWMS.Otpremnica(Id, Sifra, Izdano, SkladisteId, DatumKreiranja,DatumOtpremanja)
VALUES (2, '00003', 'Vlakovi d.o.o', 1, current_timestamp,'2016-05-03');

INSERT INTO HWMS.OtpremnicaProizvod(Id, ProizvodId, OtpremnicaId, Kolicina, DatumKreiranja)
VALUES (1, 1, 1, 1, current_timestamp);

INSERT INTO HWMS.OtpremnicaProizvod(Id, ProizvodId, OtpremnicaId, Kolicina, DatumKreiranja)
VALUES (2, 2, 2, 1, current_timestamp);

