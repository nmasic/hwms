CREATE TABLE HWMS.Valuta (
  Id int not null auto_increment primary key,
  SifraN char(3) NOT NULL,
  SifraS char(3) NOT NULL,
  Oznaka varchar(10) NOT NULL
);

INSERT INTO HWMS.Valuta(Id,SifraN,SifraS,Oznaka) VALUES (1,'191','HRK', 'kn');
INSERT INTO HWMS.Valuta(Id,SifraN,SifraS,Oznaka) VALUES (2,'978','EUR', '€');
INSERT INTO HWMS.Valuta(Id,SifraN,SifraS,Oznaka) VALUES (3,'840','USD', '$');


ALTER TABLE HWMS.Proizvod 
ADD (
	ValutaId int
);

ALTER TABLE HWMS.Proizvod 
ADD FOREIGN KEY(ValutaId) REFERENCES HWMS.Valuta(Id);

update HWMS.Proizvod set ValutaId=1;

