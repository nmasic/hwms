CREATE TABLE HWMS.VrstaProizvoda(
Id int not null auto_increment primary key,
Naziv varchar(100) not null
);

CREATE TABLE HWMS.Proizvod (
Id int not null auto_increment primary key,
Naziv varchar(255),
Opis varchar(255),
Cijena decimal,
DatumKreiranja datetime,
DatumIzmjene datetime,
VrstaProizvodaId int not null,
FOREIGN KEY(VrstaProizvodaId) REFERENCES HWMS.VrstaProizvoda(Id)
);

CREATE TABLE HWMS.Skladiste(
Id int not null auto_increment primary key,
Sifra varchar(20),
Naziv varchar(255),
Kapacitet int,
DatumKreiranja datetime,
DatumIzmjene datetime
);

CREATE TABLE HWMS.SkladisteProizvod(
Id int not null auto_increment primary key,
Kolicina int,
DatumKreiranja datetime,
DatumIzmjene datetime,
SkladisteId int not null,
ProizvodId int not null,
FOREIGN KEY(SkladisteId) REFERENCES HWMS.Skladiste(Id),
FOREIGN KEY(ProizvodId) REFERENCES HWMS.Proizvod(Id)
);

CREATE TABLE HWMS.Primka(
Id int not null auto_increment primary key, 	
Sifra varchar(50),
NazivDobavljaca varchar(100),
SkladisteId int not null,
DatumKreiranja datetime,
FOREIGN KEY(SkladisteId) REFERENCES HWMS.Skladiste(Id)
);

CREATE TABLE HWMS.PrimkaProizvod (
Id int not null auto_increment primary key,
ProizvodId int not null,
PrimkaId int not null,
Kolicina int,
DatumKreiranja datetime,
FOREIGN KEY(ProizvodId) REFERENCES HWMS.Proizvod(Id),
FOREIGN KEY(PrimkaId) REFERENCES HWMS.Primka(Id)
);

CREATE TABLE HWMS.Meduskladisnica(
Id int not null auto_increment primary key,
Sifra varchar(50),
SourceSkladisteId int not null,
TargetSkladisteId	int not null,	
DatumKreiranja datetime,
FOREIGN KEY(SourceSkladisteId) REFERENCES HWMS.Skladiste(Id),
FOREIGN KEY(TargetSkladisteId) REFERENCES HWMS.Skladiste(Id)
);          

CREATE TABLE HWMS.MeduskladisnicaProizvod(
Id int not null auto_increment primary key,
ProizvodId int not null,
MeduskladisnicaId int not null,
Kolicina int,
DatumKreiranja datetime,
FOREIGN KEY(ProizvodId) REFERENCES HWMS.Proizvod(Id),
FOREIGN KEY(MeduskladisnicaId) REFERENCES HWMS.Meduskladisnica(Id)
);