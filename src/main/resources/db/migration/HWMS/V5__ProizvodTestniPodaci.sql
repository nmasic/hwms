INSERT INTO HWMS.Proizvod(Id, Naziv, Opis, Cijena, DatumKreiranja, DatumIzmjene, VrstaProizvodaId, KorisnikKreiranja)
VALUES (1,'Samsung','50 inch televizor', 2000.00, '2016-05-02', null, 1, 'batch');

INSERT INTO HWMS.Proizvod(Id, Naziv, Opis, Cijena, DatumKreiranja, DatumIzmjene, VrstaProizvodaId, KorisnikKreiranja)
VALUES (2,'Lenovo','17 inch laptop', 5000.00, '2016-05-03', null, 2, 'batch');

INSERT INTO HWMS.Proizvod(Id, Naziv, Opis, Cijena, DatumKreiranja, DatumIzmjene, VrstaProizvodaId, KorisnikKreiranja)
VALUES (3,'Toshiba','15 inch laptop', 3000.00, '2016-05-07', null, 2, 'batch');

INSERT INTO HWMS.Proizvod(Id, Naziv, Opis, Cijena, DatumKreiranja, DatumIzmjene, VrstaProizvodaId, KorisnikKreiranja)
VALUES (4,'Logitech','laser - blue', 100.00, '2016-05-08', null, 3, 'batch');

INSERT INTO HWMS.SkladisteProizvod(Id, Kolicina, DatumKreiranja, DatumIzmjene, SkladisteId, ProizvodId)
values(1, 2, '2016-05-02', null, 1, 1);

INSERT INTO HWMS.SkladisteProizvod(Id, Kolicina, DatumKreiranja, DatumIzmjene, SkladisteId, ProizvodId)
values(2, 4, '2016-05-02', null, 2, 1);

INSERT INTO HWMS.SkladisteProizvod(Id, Kolicina, DatumKreiranja, DatumIzmjene, SkladisteId, ProizvodId)
values(3, 4, '2016-05-02', null, 2, 3);

INSERT INTO HWMS.SkladisteProizvod(Id, Kolicina, DatumKreiranja, DatumIzmjene, SkladisteId, ProizvodId)
values(4, 3, '2016-05-03', null, 1, 2);

INSERT INTO HWMS.Primka(Id, Sifra, NazivDobavljaca, SkladisteId, DatumKreiranja)
VALUES (1, '00001', 'Marko i sinovi', 1, '2016-05-02');

INSERT INTO HWMS.Primka(Id, Sifra, NazivDobavljaca, SkladisteId, DatumKreiranja)
VALUES (2, '00003', 'Drimia', 1, '2016-05-03');

INSERT INTO HWMS.PrimkaProizvod(Id, ProizvodId, PrimkaId, Kolicina, DatumKreiranja)
VALUES (1, 1, 1, 2, '2016-05-02');

INSERT INTO HWMS.PrimkaProizvod(Id, ProizvodId, PrimkaId, Kolicina, DatumKreiranja)
VALUES (2, 2, 2, 3, '2016-05-03');

INSERT INTO HWMS.PrimkaProizvod(Id, ProizvodId, PrimkaId, Kolicina, DatumKreiranja)
VALUES (3, 2, 1, 5, '2016-05-02');
