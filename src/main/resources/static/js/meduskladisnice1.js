    function save(){
    	var flag=false;
    
    	$("#errorText").html("");
    	$("#sifraError").html("");
    	
    	if (  $("#pSkladiste").val()==$("#oSkladiste").val() )  {
    		
    	    $("<p>Pocetno i odredisno skladiste ne mogu biti jednaki</p>").appendTo("#errorText");
    	    flag=true;
    	}
    	if  ( ($("#sifra").val().length<5)||($("#sifra").val().length>50) ){
    	    $("<p>Sifra mora biti u rasponu izmedu 5 i 50 znakova</p>").appendTo("#sifraError");
    	    flag=true;
    	}
    	return;    
         
    }
    
    
    function setSourceSkladisteIdInput(){
    	
    	$("#sourceSkladisteId").val($("#pSkladiste").val());
    	
    	
    }
 function setTargetSkladisteIdInput(){
    	
    	$("#targetSkladisteId").val($("#oSkladiste").val());
    	
    	
    }