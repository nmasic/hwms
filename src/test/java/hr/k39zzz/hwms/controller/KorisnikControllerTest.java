package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class KorisnikControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziListu() throws Exception {
		mockMvc.perform(get("/admin/korisnik/lista")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("korisnik/korisnikLista")) //
				.andExpect(model().attribute("korisnici", notNullValue()));
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziFormu() throws Exception {
		mockMvc.perform(get("/admin/korisnik/unos")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("korisnik/korisnikForm")) //
				.andExpect(model().attribute("korisnikForm", hasProperty("ime", nullValue())));

		mockMvc.perform(get("/admin/korisnik/unos") //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("korisnik/korisnikForm")) //
				.andExpect(model().attribute("korisnikForm", hasProperty("ime", notNullValue())));
	}

	@Test
	@WithUserDetails("admin")
	public void postFormOdustani() throws Exception {
		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("odustani", "odustani") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("role", "ADMIN")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/admin"));
	}

	@Test
	@WithUserDetails("admin")
	public void postFormaUspjesno() throws Exception {
		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("korisnickoIme", "korisnickoIme") //
				.param("lozinka", "lozinka") //
				.param("role", "ADMIN")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/admin/korisnik/lista"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("id", "2") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("korisnickoIme", "korisnickoIme") //
				.param("lozinka", "lozinka") //
				.param("role", "ADMIN")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/admin/korisnik/lista"));

	}

	@Test
	@WithUserDetails("admin")
	public void postFormaDuploKorisnicko() throws Exception {

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("korisnickoIme", "duploIme") //
				.param("lozinka", "lozinka") //
				.param("role", "ADMIN")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/admin/korisnik/lista"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("korisnickoIme", "duploIme") //
				.param("lozinka", "lozinka") //
				.param("role", "ADMIN")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"));

	}

	@Test
	@WithUserDetails("admin")
	public void postFormaObavezniPodaci() throws Exception {
		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "prezime"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "ime"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "email"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "ime"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("korisnickoIme", "korisnickoIme") //
				.param("lozinka", "lozinka")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "role"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("role", "ADMIN")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "korisnickoIme"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("role", "ADMIN") //
				.param("korisnickoIme", "korisnickoIme")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "lozinka"));

		mockMvc.perform(post("/admin/korisnik/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("ime", "ime") //
				.param("prezime", "prezime") //
				.param("email", "email") //
				.param("role", "ADMIN") //
				.param("korisnickoIme", "korisnickoIme")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/korisnikForm"))
				.andExpect(model().attributeHasFieldErrors("korisnikForm", "lozinka"));
	}

}
