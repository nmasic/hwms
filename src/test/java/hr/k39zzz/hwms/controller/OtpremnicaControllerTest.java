package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class OtpremnicaControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Test
	@WithUserDetails("korisnik")
	public void dohvatiListuOtpremnica() throws Exception {
		mockMvc.perform(get("/otpremnica/index")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaLista")) //
				.andExpect(model().attribute("otpremnicaList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void dohvatiListuOtpremnicaNaUpit() throws Exception {
		mockMvc.perform(get("/otpremnica/upit/izdano") //
				.param("izdano", "Marko i kćeri")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaLista")) //
				.andExpect(model().attribute("otpremnicaList", notNullValue())) //
				.andExpect(model().attribute("otpremnica1", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void prikazFormeZaUnosOtpremnice() throws Exception {
		mockMvc.perform(get("/otpremnica/unos")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos")) //
				.andExpect(model().attribute("otpremnicaUnos", hasProperty("izdano", nullValue())));

		mockMvc.perform(get("/otpremnica/unos") //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos")) //
				.andExpect(model().attribute("otpremnicaUnos", notNullValue())) //
				.andExpect(model().attribute("naslov", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void odustaniOdUnosaOtpremnice() throws Exception {
		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("odustani", "odustani"))//
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/otpremnica/index"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void potvrdaUnosaOtpremniceGreska() throws Exception {

		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "00001") //
				.param("izdano", "Mario") //
				.param("skladisteId", "1"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos"));

		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "000") //
				.param("izdano", "M") //
				.param("skladisteId", "1"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos"));

		String sifra = null;
		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", sifra) //
				.param("izdano", "Marko") //
				.param("skladisteId", "1"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos"));

		String izdano = null;
		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "00001") //
				.param("izdano", izdano) //
				.param("skladisteId", "1"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaUnos"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void potvrdaUnosaOtpremniceIspravno() throws Exception {
		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "00005") //
				.param("izdano", "Mario") //
				.param("skladisteId", "2"))//
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/otpremnica/index"));

		mockMvc.perform(post("/otpremnica/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("id", "2") //
				.param("sifra", "00008") //
				.param("izdano", "Marijana d.o.o.") //
				.param("skladisteId", "2"))//
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/otpremnica/index"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void prikazListeProizvodaZaOtpremnicu() throws Exception {
		mockMvc.perform(get("/otpremnica/otpremnicaProizvodi") //
				.param("id", "2")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaProizvodi")) //
				.andExpect(model().attribute("otpremnica", notNullValue())) //
				.andExpect(model().attribute("otpremnicaProizvodList", notNullValue()))
				.andExpect(model().attribute("naslov", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void stornoProizvodaOtpremnice() throws Exception {
		mockMvc.perform(get("/otpremnica/stornoProizvodaOtpremnice") //
				.param("id", "1")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/otpremnica/index"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void prikazFormeZaUnosProizvodaOtpremnice() throws Exception {
		mockMvc.perform(get("/otpremnica/otpremnicaProizvodUnos") //
				.param("id", "2")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaProizvodUnos")) //
				.andExpect(model().attribute("otpremnica", notNullValue())) //
				.andExpect(model().attribute("skladisteList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void potvrdaUnosaOtpremniceProizvodaGreska() throws Exception {
		mockMvc.perform(post("/otpremnica/otpremnicaProizvodUnos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("id", "1").param("sifra", "00005") //
				.param("izdano", "Mario") //
				.param("OtpremnicaProizvodList[0].proizvod", "2").param("OtpremnicaProizvodList[0].id", "1")
				.param("OtpremnicaProizvodList[0].kolicina", "0").param("skladisteId", "3"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaProizvodUnos"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void potvrdaUnosaOtpremniceProizvodaIspravno() throws Exception {
		mockMvc.perform(post("/otpremnica/otpremnicaProizvodUnos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("id", "1").param("sifra", "00005") //
				.param("izdano", "Mario") //
				.param("OtpremnicaProizvodList[0].proizvod", "2").param("OtpremnicaProizvodList[0].id", "1")
				.param("OtpremnicaProizvodList[0].kolicina", "2").param("skladisteId", "2"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaProizvodi"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void dodajProizvodNaOtpremnicu() throws Exception {
		mockMvc.perform(post("/otpremnica/otpremnicaProizvodUnos").with(csrf()) //
				.param("addRow", "addRow"))//
				.andExpect(status().isOk()) //
				.andExpect(view().name("otpremnica/otpremnicaProizvodUnos"))//
				.andExpect(model().attribute("otpremnica", notNullValue())); // ;
	}

}
