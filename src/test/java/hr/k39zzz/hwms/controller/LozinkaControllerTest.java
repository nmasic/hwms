package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Korisnik;
import hr.k39zzz.hwms.model.KorisnikRola;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class LozinkaControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new InMemoryUserDetailsManager(Arrays.asList(admin()));
	}

	private Korisnik admin() {
		Korisnik admin = new Korisnik();
		admin.setKorisnickoIme("admin");
		Set<KorisnikRola> role = new HashSet<>();
		role.add(new KorisnikRola("ADMIN"));
		admin.setRole(role);
		return admin;
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziFormu() throws Exception {
		mockMvc.perform(get("/korisnik/lozinka")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("korisnik/lozinkaForm")) //
				.andExpect(model().attribute("lozinkaForm", hasProperty("trenutnaLozinka", nullValue())));
	}

	@Test
	@WithUserDetails("admin")
	public void postFormaUspjesno() throws Exception {
		mockMvc.perform(post("/korisnik/lozinka").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("trenutnaLozinka", "admin") //
				.param("novaLozinka", "aaa") //
				.param("novaLozinkaPotvrda", "aaa")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/korisnik/lozinka/uspjesno"));

	}
	
	@Test
	@WithUserDetails("admin")
	public void postFormaOdustani() throws Exception {
		mockMvc.perform(post("/korisnik/lozinka").with(csrf()) //
				.param("odustani", "odustani") //
				.param("trenutnaLozinka", "admin") //
				.param("novaLozinka", "aaa") //
				.param("novaLozinkaPotvrda", "aaa")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/pocetna"));

	}

	@Test
	@WithUserDetails("admin")
	public void postFormaTrenutnaLozinkaKriva() throws Exception {
		mockMvc.perform(post("/korisnik/lozinka").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("trenutnaLozinka", "admin111") //
				.param("novaLozinka", "aaa") //
				.param("novaLozinkaPotvrda", "aaa")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/lozinkaForm"))
				.andExpect(model().attributeHasFieldErrors("lozinkaForm", "trenutnaLozinka"));
	}

	@Test
	@WithUserDetails("admin")
	public void postFormaNoveLozinkeNisuJednake() throws Exception {
		mockMvc.perform(post("/korisnik/lozinka").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("trenutnaLozinka", "admin") //
				.param("novaLozinka", "aaa") //
				.param("novaLozinkaPotvrda", "aaaa")) //
				.andExpect(status().isOk()).andExpect(view().name("korisnik/lozinkaForm"))
				.andExpect(model().attributeHasFieldErrors("lozinkaForm", "novaLozinkaPotvrda"));

	}

	@Test
	public void postFormaKorisnikNijePrijavljen() throws Exception {
		mockMvc.perform(post("/korisnik/lozinka").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("trenutnaLozinka", "admin") //
				.param("novaLozinka", "aaa") //
				.param("novaLozinkaPotvrda", "aaa")) //
				.andExpect(status().isFound());

	}

}
