package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class PrimkaControllerTest {
	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetIzbornik() throws Exception {
		mockMvc.perform(get("/izbornik.html")).andExpect(status().isOk()).andExpect(view().name("izbornik"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetLista() throws Exception {
		mockMvc.perform(get("/primke/index")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeLista")) //
				.andExpect(model().attribute("primkeList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetProizvodiSkladista() throws Exception {
		mockMvc.perform(get("/primka/upit/nazivDobavljaca").with(csrf()) //
				.param("nazivDobavljaca", "Drimia")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeLista")) //
				.andExpect(model().attribute("primka1", notNullValue())) //
				.andExpect(model().attribute("primkeList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetFormWithoutId() throws Exception {
		mockMvc.perform(get("/primke/unos")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeUnos")) //
				.andExpect(model().attribute("skladisteList", notNullValue()))
				.andExpect(model().attribute("primkaUnos", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetFormWithId() throws Exception {
		mockMvc.perform(get("/primke/unos").with(csrf()) //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeUnos")) //
				.andExpect(model().attribute("skladisteList", notNullValue())) //
				.andExpect(model().attribute("primkaUnos", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testOdustaniFormPrimka() throws Exception {
		mockMvc.perform(post("/primke/unos").with(csrf()) //
				.param("odustani", "1")) //
				.andExpect(status().is3xxRedirection()) //
				.andExpect(view().name("redirect:/primke/index"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testPotvrdiFormPrimka() throws Exception {
		mockMvc.perform(post("/primke/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "00009") //
				.param("skladisteId", "1") //
				.param("nazivDobavljaca", "test")) //
				.andExpect(status().is3xxRedirection()) //
				.andExpect(view().name("redirect:/primke/proizvodi?id=3"));

		mockMvc.perform(post("/primke/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("sifra", "009") //
				.param("skladisteId", "1") //
				.param("nazivDobavljaca", "test")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeUnos"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetProizvodi() throws Exception {
		mockMvc.perform(get("/primke/proizvodi") //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeProizvodi")) //
				.andExpect(model().attribute("primka", notNullValue())) //
				.andExpect(model().attribute("enableEdit", notNullValue())) //
				.andExpect(model().attribute("naslov", notNullValue())) //
				.andExpect(model().attribute("primkaProizvodList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testOdustaniFormPrimkaProizvod() throws Exception {
		mockMvc.perform(post("/primke/unos").with(csrf()) //
				.param("odustani", "odustani")) //
				.andExpect(status().is3xxRedirection()) //
				.andExpect(view().name("redirect:/primke/index"));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testGetFormProizvod() throws Exception {
		mockMvc.perform(get("/primke/unosPrimkeProizvoda") //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeProizvodUnos")) //
				.andExpect(model().attribute("primka", notNullValue())) //
				.andExpect(model().attribute("skladisteList", notNullValue()));
	}

	@Test
	@WithUserDetails("korisnik")
	public void testAddRow() throws Exception {
		mockMvc.perform(post("/primke/primkeProizvodUnos").with(csrf()) //
				.param("addRow", "addRow")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("primke/primkeProizvodUnos"));
	}
}
