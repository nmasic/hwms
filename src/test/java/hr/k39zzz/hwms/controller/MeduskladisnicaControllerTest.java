package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Korisnik;
import hr.k39zzz.hwms.model.KorisnikRola;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration

public class MeduskladisnicaControllerTest {
	
	
	
	
	
	
	
	
	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
		return new InMemoryUserDetailsManager(Arrays.asList(admin()));
		
		
		
		
	}

	private Korisnik admin() {
		Korisnik admin = new Korisnik();
		admin.setKorisnickoIme("admin");
		Set<KorisnikRola> role = new HashSet<>();
		role.add(new KorisnikRola("ADMIN"));
		admin.setRole(role);
		return admin;
	}
	@Test
	@WithUserDetails("admin")
	public void showAllMeduskladisnica() throws Exception {
		mockMvc.perform(get("/meduskladisnice/lista")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("meduskladisnice/listaMeduskladisnica")) //
				.andExpect(model().attribute("meduskladisnice", notNullValue()));
	}
	
	
	@Test
	@WithUserDetails("admin")
	public void prikaziDetaljeMeduskladisnice() throws Exception {
		mockMvc.perform(get("/meduskladisnica/detalji") //
				.param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("meduskladisnice/detaljiMeduskladisnica")) //
				.andExpect(model().attribute("listaProizvoda", notNullValue())) //
				.andExpect(model().attribute("listaKolicina", notNullValue())) //
				.andExpect(model().attribute("size", notNullValue()));
	}
	
	
	
	@Test
	@WithUserDetails("admin")
	public void prikaziIzbornik() throws Exception {
		mockMvc.perform(get("/meduskladisnica/promijeni")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("meduskladisnice/promijeniSkladiste")) //
				.andExpect(model().attribute("pocetnaSkladista", notNullValue()))
		        .andExpect(model().attribute("odredisnaSkladista", notNullValue()));
	}
	
	
	
	
	
	
	
	

}


