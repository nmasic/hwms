package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class ProizvodStanjeControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziPrvuStranicu() throws Exception {
		mockMvc.perform(get("/skladiste/stanje/kolicinsko/skladista")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("skladiste/stanje/kolicinskoStanje")) //
				.andExpect(model().attribute("svaSkladista", notNullValue()));
	}

	@Test
	@WithUserDetails("admin")
	public void proizvodiZaIzabranoSkladiste() throws Exception {
		mockMvc.perform(get("/skladiste/stanje/kolicinsko/proizvodi") //
				.param("skid", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("skladiste/stanje/kolicinskoStanje")) //
				.andExpect(model().attribute("svaSkladista", notNullValue())) //
				.andExpect(model().attribute("proizvodList", notNullValue()));
	}

	@Test
	@WithUserDetails("admin")
	public void proizvodSDokumentima() throws Exception {
		mockMvc.perform(get("/skladiste/stanje/kolicinsko/proizvod") //
				.param("skid", "1") //
				.param("pid", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("skladiste/stanje/kolicinskoStanjeProizvod")) //
				.andExpect(model().attribute("dokumenti", notNullValue())) //
				.andExpect(model().attribute("skladiste", notNullValue())) //
				.andExpect(model().attribute("proizvod", notNullValue()));
	}
}
