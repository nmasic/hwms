package hr.k39zzz.hwms.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.k39zzz.hwms.HwmsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class ProizvodControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).defaultRequest(get("/")).build();
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziListu() throws Exception {
		mockMvc.perform(get("/proizvod/index")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("proizvod/proizvodLista")) //
				.andExpect(model().attribute("proizvodList", notNullValue()));
	}

	@Test
	@WithUserDetails("admin")
	public void prikaziFormu() throws Exception {
		mockMvc.perform(get("/admin/proizvod/unos")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("proizvod/proizvodUnos")) //
				.andExpect(model().attribute("proizvodUnos", hasProperty("id", nullValue())));

		mockMvc.perform(get("/admin/proizvod/unos").param("id", "1")) //
				.andExpect(status().isOk()) //
				.andExpect(view().name("proizvod/proizvodUnos")) //
				.andExpect(model().attribute("proizvodUnos", hasProperty("id", notNullValue())));
	}

	@Test
	@WithUserDetails("admin")

	public void postFormaUspjesno() throws Exception {
		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("naziv", "naziv") //
				.param("opis", "opis") //
				.param("vrstaProizvoda.id", "2") //
				.param("cijena", "34234.45")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/proizvod/index"));

		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("id", "1") //
				.param("naziv", "naziv") //
				.param("opis", "opis") //
				.param("vrstaProizvoda.id", "2") //
				.param("cijena", "34234.45")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/proizvod/index"));

	}

	@Test
	@WithUserDetails("admin")
	public void postFormaOdustani() throws Exception {
		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("odustani", "odustani") //
				.param("naziv", "nazivOdustani") //
				.param("opis", "opis") //
				.param("vrstaProizvoda.id", "2") //
				.param("cijena", "34234.45")) //
				.andExpect(status().isFound()) //
				.andExpect(view().name("redirect:/proizvod/index"));
	}

	@Test
	@WithUserDetails("admin")
	public void postFormaVrstaProizvoda() throws Exception {

		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("naziv", "naziv") //
				.param("opis", "opis") //
				.param("cijena", "34234.45")) //
				.andExpect(status().isOk()).andExpect(view().name("proizvod/proizvodUnos"))
				.andExpect(model().attributeHasFieldErrors("proizvodUnos", "vrstaProizvoda"));

	}

	@Test
	@WithUserDetails("admin")
	public void postFormaObavezniPodaci() throws Exception {

		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("opis", "opis") //
				.param("vrstaProizvoda.id", "2") //
				.param("cijena", "34234.45")) //
				.andExpect(status().isOk()).andExpect(view().name("proizvod/proizvodUnos"))
				.andExpect(model().attributeHasFieldErrors("proizvodUnos", "naziv"));

		mockMvc.perform(post("/admin/proizvod/unos").with(csrf()) //
				.param("potvrdi", "potvrdi") //
				.param("naziv", "naziv") //
				.param("opis", "opis") //
				.param("cijena", "cijena")) //
				.andExpect(status().isOk()).andExpect(view().name("proizvod/proizvodUnos"))
				.andExpect(model().attributeHasFieldErrors("proizvodUnos", "cijena"));

	}
}
