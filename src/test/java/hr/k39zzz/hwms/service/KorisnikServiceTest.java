package hr.k39zzz.hwms.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Korisnik;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class KorisnikServiceTest {

	@Autowired
	private KorisnikService korisnikService;

	@Test
	public void promjenaLozinka() {

		String korisnickoIme = "korisnik";
		String novaLozinka = "dasdadssda";

		Korisnik korisnik = korisnikService.findByKorisnickoIme(korisnickoIme);
		Assert.assertNotNull(korisnik);

		korisnikService.updateLozinka(novaLozinka, korisnickoIme);

		Korisnik korisnikPromijenjen = korisnikService.findByKorisnickoIme(korisnickoIme);
		Assert.assertNotNull(korisnikPromijenjen);
		Assert.assertFalse(korisnikService.lozinkeNeOdgovaraju(novaLozinka, korisnikPromijenjen.getLozinka()));
	}

}
