package hr.k39zzz.hwms.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Skladiste;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class SkladisteServiceTest{ 
	
	
	@Autowired
	private SkladisteService skladisteService;
	
	
	@Test
	public void testIfSkladisteServiceReturnsObjects(){
		
		List<Skladiste> skladista=skladisteService.fetchAllSkladiste();
		Assert.assertNotNull(skladista);
		
		
	}
	
	
	@Test
	public void testIfSkladisteServiceReturnsObjectById(){
		
		Skladiste skladiste=skladisteService.fetchSkladisteById(1);
		Assert.assertNotNull(skladiste);
		
		
	}
	
	
	@Test
	public void testIfSkladisteServiceReturnedObjectByIdIsValid(){
		
		Skladiste skladiste=skladisteService.fetchSkladisteById(1);
		Assert.assertSame(skladiste.getId(),1);
		
		
	}

	
	
		

}
