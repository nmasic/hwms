package hr.k39zzz.hwms.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Meduskladisnica;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class MeduskladisnicaServiceTest {
	
	
	@Autowired
	private MeduskladisnicaService meduskladisnicaService;
	
	
	
	
	

	@Test
	public void testIfServiceReturnsNonNullableList(){
		
		List<Meduskladisnica> meduskladisnice=meduskladisnicaService.fetchAllMeduskladisnica();
	
	     Assert.assertNotNull(meduskladisnice);
	}

	
	@Test
	public void testIfReturnedObjectByIdIsValid(){
		
		
		Meduskladisnica meduskladisnica=meduskladisnicaService.findMeduskladisnicaById(1);
		Assert.assertSame(meduskladisnica.getId(), 1);
	}
	
	
}
