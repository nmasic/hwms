package hr.k39zzz.hwms.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.dto.DokumentDto;
import hr.k39zzz.hwms.model.dto.DokumentDto.VrstaDokument;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class ProizvodServiceTest {

	@Autowired
	private ProizvodService proizvodService;

	@Test
	public void daLiSuSviDokumentiIzvjestaj() {

		List<DokumentDto> listaDokumenata = proizvodService.sviDokumentZaProizvodSkladiste(1, 2);

		Assert.assertNotNull(listaDokumenata);
		Assert.assertEquals(4, listaDokumenata.size());

		DokumentDto ukupno = listaDokumenata.get(listaDokumenata.size() - 1);
		Assert.assertEquals(VrstaDokument.UKUPNO, ukupno.getVrsta());
		Assert.assertEquals(8, ukupno.getUlaz());
		Assert.assertEquals(1, ukupno.getIzlaz());

	}

}
