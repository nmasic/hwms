package hr.k39zzz.hwms;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class HwmsApplicationTests {

	@Autowired
	private JdbcTemplate template;

	@Test
	public void provjeraOsnovnihPostavka() throws Exception {
		assertEquals("ADMIN", this.template.queryForObject("SELECT Rola from KorisnikRola where id = 1", String.class));
	}

}
