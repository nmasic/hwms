package hr.k39zzz.hwms.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.MeduskladisnicaProizvod;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class MeduskladisnicaProizvodRepositoryTest {
	
	
	
	@Autowired
	MeduskladisnicaProizvodRepository meduskladisnicaProizvodRepository;
	
	
	
	
	
	@Test
	public void testIfRepositoryReturnsObjects(){
		
		List<MeduskladisnicaProizvod> lista= meduskladisnicaProizvodRepository.findAll();
		assertNotNull(lista);
	}
	/*
	 * 
	 * lambda expression used in assertrue
	 * 
	 * 
	 */
    @Test
    public void testIfListContainsMoreThanOneObject(){
    	
    	List<MeduskladisnicaProizvod> lista= meduskladisnicaProizvodRepository.findAll();
    	assertTrue(lista.stream().count()>1);
    }
    @Test
    public void testIfMeduskladisnicaProizvodObjectIsValid(){
    	
    	
    	MeduskladisnicaProizvod meduskladisnicaProizvod=meduskladisnicaProizvodRepository.findOne(1);
    	
    	assertSame(meduskladisnicaProizvod.getKolicina(),4);
    	
    }
    
}