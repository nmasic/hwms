package hr.k39zzz.hwms.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Otpremnica;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class OtpremnicaRepositoryTest {

	@Autowired
	private OtpremnicaRepository otpremnicaRepository;

	@Autowired
	private OtpremnicaProizvodRepository otpremnicaProizvodRepository;

	@Test
	public void dohvatiSveOtpremniceIzBaze() throws Exception {
		List<Otpremnica> otpremnicaList = otpremnicaRepository.findAll();
		assertNotNull(otpremnicaList);
		assertSame(otpremnicaList.size(), 2);

	}

	@Test
	public void dohvatiOtpremnicuIzBaze() throws Exception {
		Otpremnica otpremnica = otpremnicaRepository.findFirstBySifra("00001");
		assertNotNull(otpremnica);
		assertSame(otpremnica.getSkladisteId(), 1);

	}

	@Test
	public void dohvatiOtpremnicuIzBaze2() throws Exception {
		Otpremnica otpremnica = otpremnicaRepository.findFirstBySifra("00001");
		assertNotNull(otpremnica);
		assertEquals(otpremnica.getSifra(), "00001");
		// assertSame(otpremnica.getSifra(), "00001");
	}

	@Test
	public void dohvatiListuOtpremnicaIzBaze() throws Exception {
		List<Otpremnica> otpremnicaList = otpremnicaRepository.findOtpremnicaByIzdanoIgnoreCaseContaining("r");
		assertNotNull(otpremnicaList);
		assertSame(otpremnicaList.size(), 1);
		assertEquals("Marko i kćeri", otpremnicaList.get(0).getIzdano());
	}

	@Test
	public void dohvatiListuOtpremnicaIzBaze2() throws Exception {
		List<Otpremnica> otpremnicaList = otpremnicaRepository.findOtpremnicaByIzdanoIgnoreCaseContaining("k");
		assertNotNull(otpremnicaList);
		assertSame(otpremnicaList.size(), 2);
	}

	@Test
	public void dohvatiKolicinuZaSkladiste1() throws Exception {
		List<Object[]> otpremnicaPoMjesecima = otpremnicaProizvodRepository.sumOtpremnicaPerMonthByYear(2016);
		assertNotNull(otpremnicaPoMjesecima);
		assertSame(otpremnicaPoMjesecima.size(), 1);
	}

}
