package hr.k39zzz.hwms.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Proizvod;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class ProizvodRepositoryTest {

	
	@Autowired
	private ProizvodRepository proizvodRepository;


	@Test
	public void dohvatiSveProizvodeIzBaze() throws Exception {
		List<Proizvod> proizvodList = proizvodRepository.findAll();
		assertNotNull(proizvodList);
	}

	@Test
	public void dohvatiProizvodIzBaze() throws Exception {
		Proizvod proizvod = proizvodRepository.findOne(1);
		assertNotNull(proizvod);
		assertSame(proizvod.getId(), 1);

	}
	
}
