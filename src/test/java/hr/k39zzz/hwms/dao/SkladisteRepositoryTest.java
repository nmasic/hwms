package hr.k39zzz.hwms.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Skladiste;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class SkladisteRepositoryTest {
	@Autowired
	SkladisteRepository skladisteRepository;
	
	
	
	@Test
	public void testIfSkladisteRepositoryReturnsObjectsOrObject(){
		
		List<Skladiste> skladisteList=skladisteRepository.findAll();
		assertNotNull(skladisteList);	
		
		
	}
	
	/*
	 * 
	 * u asertTrue koristen Lambda Izraz
	 * inicijalizira se stream i zatvara se sa count
	 * 
	 * 
	 * 
	 */
	
	@Test
	public void testIfSkladisteRepositoryReturnsMoreThanOneObject(){
		
		
		List<Skladiste>  skladisteList=skladisteRepository.findAll();
		assertTrue(skladisteList.stream().count()>1);
		
		}
	
	
	
	
	@Test
	public void testIfSkladisteObjectIsValid(){
		
		
		Skladiste skladiste=skladisteRepository.findOne(1);
		assertSame(skladiste.getId(),1);
		
	}
	
	
	
	

}
