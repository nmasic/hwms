package hr.k39zzz.hwms.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import hr.k39zzz.hwms.HwmsApplication;
import hr.k39zzz.hwms.model.Primka;
import hr.k39zzz.hwms.model.PrimkaProizvod;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HwmsApplication.class)
@WebAppConfiguration
public class PrimkaRepositoryTest {

	@Autowired
	private PrimkaRepository primkaRepository;

	@Autowired
	private PrimkaProizvodRepository primkaProizvodRepository;

	@Test
	public void testFindAllByProizvodAndSkladiste() throws Exception {
		List<Object[]> primkaList = primkaRepository.findAllByProizvodAndSkladiste(1, 1);
		assertNotNull(primkaList);
	}

	@Test
	public void testFindByPrimkaId() throws Exception {
		List<PrimkaProizvod> ppList = primkaProizvodRepository.findByPrimkaId(1);
		assertNotNull(ppList);
		assertSame(2, ppList.size());
	}

	@Test
	public void testFindByNazivDobavljacaLike() throws Exception {
		List<Primka> primkaList = primkaRepository.findByNazivDobavljacaLike("Marko%");
		assertNotNull(primkaList);
		assertSame(1, primkaList.size());
	}

	@Test
	public void testFindBySifra() throws Exception {
		Primka primka = primkaRepository.findBySifra("00001");
		assertNotNull(primka);
	}

	@Modifying
	@Transactional
	@Test
	public void testUpdatePrimka() throws Exception {
		primkaRepository.updatePrimka("00001", "noviDobavljac", 1, 1);
		Primka primka = primkaRepository.findBySifra("00001");
		assertSame("noviDobavljac", primka.getNazivDobavljaca());
	}
}
