# README #

### Hamburg warehouse management system  ###

Projekt tima Hamburg na predmetu Napredno programiranje u programskom jeziku Java, TVZ.

### Spring Boot - http://projects.spring.io/spring-boot/ ###

Aplikacija se pokreće kao Java aplikacija:

__HwmsApplication.java je klasa sa main metodom. Desni klik na tu klasu i Run As Java Application.__

Nakon što je aplikacija pokrenuta pristupa joj s linkom:
__http://localhost:8097/hwms__


__Sve postavke za aplikaciju nalaze se u application.properties.__

Popis svih postavka koje se mogu postaviti:

http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html



### Thymeleaf -  http://www.thymeleaf.org/ ###

Sve template stranice nalaze se u src/main/resources/templates/.
Napravljen je osnovni layout (layouts/layout.html) i header (fragments/header.html).


### Bootstrap - http://getbootstrap.com/ ###

Postavljena je osnovna Bootstrap tema.

### BAZA ###
Trenutno je postavljena embedded H2. (u postavkama su stavljene i opcije za spajanje na MySQL.)

### Flyway - https://flywaydb.org/getstarted/how ###

Flyway je library koji omogućava migraciju baze na slični način kako to radi Entity Framework.

Trenutno je postavljena inicijalna skripta u kojoj se kreira tablica korisnika i rola zajedno s unosom jednog administratora.

Skripta se nalazi na lokaciji src/main/resources/db/migration/HWMS.

U slučaju promjene na bazi trebaju se dodavati nove migracije (jednostavno se doda skripta V2__naziv.sql).

### JPA/HIBERNATE - http://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/ ###
Trenutno je u DAO sloju postavljen spring-data JPA sučelje koje kao implemetaciju koristi Hibernate.
Još uvijek se može postaviti da se koristi direktno Hibernate ako se pokaže potreba za tim.